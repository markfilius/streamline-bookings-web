/* Import your third-Party plugins here */

import 'js/modernizr.js';
import 'js/jquery.js';
import 'js/perfect-scrollbar.js';
import 'bower_components/jquery-ui/jquery-ui.js';
import 'bower_components/jspdf/dist/jspdf.min.js';

// Required only to load Rubix
import 'bower_components/d3/d3.js';

// Date functions
import 'bower_components/moment/min/moment.min.js';
import 'bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js';

// Must be last
import 'js/rubix.js';

