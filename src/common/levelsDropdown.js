// import { env } from '../environment';
import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import {
	MenuItem,
	DropdownButton,
} from '@sketchpixy/rubix';
let _ = require('lodash');


class LevelsDropdown extends React.Component {

	constructor(props) {

		super(props);

		this.state = {
			levels: props.levels || [],
			level: props.level || {},
			selectCallback: props.onSelect || null,
			title: props.title || 'Choose Level...',
			dropup: false,
		}

		this.axiosInstance = null;

		// Get the levels. Possibly there isn't a token yet, so this call will be empty. That get's fixed in DidUpdate
		this.getLevels(props);
	}

	componentDidUpdate(prevProps, presState) {

		// this.setState({
		// 	levels: props.levels,
		// 	selectCallback: props.onSelect,
		// });

		// Token is added from Redux store through props, possibly a short while after mounting
		if (prevProps.token !== this.props.token) this.getLevels(this.props);
	}

	//
	// Level functions
	//
	getLevels(props) {

		let token = props && props.token;
		if (!token) return;

		let environment = props && props.environment;
		if (_.isEmpty(environment)) return;

		// Get my list of levels
		this.axiosInstance = axios.create({
			baseURL: environment.baseUri,
			headers: {token: token}
		});
		this.axiosInstance.post('/levels')
			.then(response => {
				this.setState({
						levels: response.data.levels
					});
			})
			.catch((response) => {
				console.log('levelsdropdown error response', response.response);
			});
	}

	//
	// General functions
	//
	updateState(props) {
		if (!props) return;

		this.setState((prev, props) => {
			let state = {};
			if (props.levels)         state.levels = props.levels;
			if (props.onSelect)       state.selectCallback = props.onSelect;
			return state;
		});
	}

	//
	// Renderers
	//
	render() {

		const levelsDropdown = () => {

			let key='key';

			const allLevels = this.state.levels.map(level => {
				return (
					<MenuItem
						key={ level.id }
						eventKey={ level.id }
						onSelect={ this.state.selectCallback }>
						{ level.name }
					</MenuItem>
				);
			});

			return (
				<DropdownButton bsStyle="slbprimary" title={this.state.title} key={key} id={'dropdown-basic'}>
					<MenuItem
						key='none'
						eventKey='none'
						onSelect={ this.state.selectCallback }>
						(New)
					</MenuItem>
					<MenuItem divider />
					{ allLevels }
				</DropdownButton>
			)
		}

		return (
			<div>
				{ levelsDropdown() }
			</div>
		);
	}
}

const mapStateToProps = state => {
	return state;
}

LevelsDropdown = connect (
	mapStateToProps
)(LevelsDropdown)

export default LevelsDropdown


