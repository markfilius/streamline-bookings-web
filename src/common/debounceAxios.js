

let _waiting = false;
let _payload = {};
let defaultTimeout = 4000;


export default function debounceAxios (axiosInstance, method, route, payload, successCallback, errorCallback, timeout) {

	// Debounce the axios call: collect the payload and send on timeout
	// Note: payload entered WHILE sending will be lost

	_payload = payload;

	if (!_waiting) {
		setTimeout(() => {
				_waiting = false;
				axiosInstance[method](route, _payload)
					.then(response => successCallback(response))
					.catch(response => errorCallback(response))
			}
			, timeout || defaultTimeout);
	}
	_waiting = true;
}
