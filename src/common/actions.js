// import * as logger from '../common/logger';


/*
 * action types
 */
export const SET_ENVIRONMENT = 'SET_ENVIRONMENT';
export const SET_SCHOOL = 'SET_SCHOOL';
export const SET_VENUE = 'SET_VENUE';
export const SET_PERSON = 'SET_PERSON';
export const SET_TOKEN = 'SET_TOKEN';
export const SET_NOTIFY = 'SET_NOTIFY';

/*
 * action creators
 */
export function setEnvironment(environment) {
	return { type: SET_ENVIRONMENT, environment }
}

export function setSchool(school) {
	return { type: SET_SCHOOL, school }
}

export function setVenue(venue) {
	return { type: SET_VENUE, venue }
}

export function setPerson(person) {
	return { type: SET_PERSON, person }
}

export function setToken(token) {
	return { type: SET_TOKEN, token };
}

export function notify(message, options) {
	// console.log('HERE AT ACTION', message, options);
	return { type: SET_NOTIFY, message, options };
}

/*
 * Reducers
 */
export function Slb(state = {}, action) {
	// console.log('HERE AT REDUCER', state, action);
	switch (action.type) {
		case SET_ENVIRONMENT:
			return Object.assign({}, state, setEnvironment(action.environment));

		case SET_SCHOOL:
			return Object.assign({}, state, setSchool(action.school));

		case SET_VENUE:
			return Object.assign({}, state, setVenue(action.venue));

		case SET_PERSON:
			return Object.assign({}, state, setPerson(action.person));

		case SET_TOKEN:
			return Object.assign({}, state, setToken(action.token));

		case SET_NOTIFY:
			return Object.assign({}, state, notify(action.message, action.options));

		default:
			return state;
	}
}

