import React from 'react';

import {
	Row,
	Col,
	Grid,
} from '@sketchpixy/rubix';

export default class Footer extends React.Component {

	state = {
		version: '1.2',
	};

	render() {
		var year = new Date().getFullYear();
		return (
			<div id='footer-container'>
				<Grid id='footer' className='text-center'>
					<Row>
						<Col xs={ 12 }>
							<div>© { year } Streamline Bookings - version { this.state.version }</div>
						</Col>
					</Row>
				</Grid>
			</div>
		);
	}
}
