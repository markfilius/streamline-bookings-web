/*
 * Notifications through email or sms to groups like staff or instructors
 */

// Third party imports
import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import DatePicker from 'react-datepicker';
import { validate } from 'validate.js';
import {
	Row,
	Col,
	DropdownHoverButton,
	Icon,
	Grid,
	MenuItem,
	Panel,
	PanelBody,
	PanelContainer,
	Form,
	FormGroup,
	FormControl,
	ControlLabel,
	Checkbox,
	Button,
	InputGroup,
	ButtonGroup,
} from '@sketchpixy/rubix';

let moment = require('moment');
import Dropzone from 'react-dropzone'

// Own imports
import { env } from '../environment';
import * as logger from '../common/logger';


class Notify extends React.Component {

	constructor(props) {

		super(props);
		logger.info('NOTIFYCONST', props);

		this.state = {
			content: '',
			from: props.person && props.person.email,
			notifyStaff: props.route.detail == 'staff' ? true : false,
			subject: '',
			toAdmins: false,
			toInstructors: false,
			uploadMessage: '',
			uploaded: [],
		}

		this.axiosInstance = null;

		this.attachmentsMaxSize = 0.5 * 1024 * 1024;
		this.attachmentsAllowedTypes = ['image/jpeg', 'image/png', 'application/pdf'];

		// Bind 'this' to local functions
		this.handleFormChange = this.handleFormChange.bind(this);
		this.handleDropZone = this.handleDropZone.bind(this);
		this.sendEmail = this.sendEmail.bind(this);
	}

	componentDidMount(props) {
		logger.info('NOTIFYDIDMOUNT', props, this.state);

		let environment = props && props.environment;
		if (_.isEmpty(environment)) return;

		this.axiosInstance = axios.create({
			baseURL: environment.baseUri,
			headers: {token: props.token},
		});
	}

	componentDidUpdate(prevProps, prevState) {
		logger.info('NOTIFYDIDUPDATE');
		if (prevProps.person.email !== this.props.person.email) {
			this.setState({
				from: props.person.email,
			});
		}
	}

	handleFormChange(event) {

		let newValue = {
			[event.target.id]: event.target.type === 'checkbox' ? event.target.checked : event.target.value
		};
 		this.setState(newValue);
	}

	handleDropZone(accepted, rejected) {
		logger.info('Handle Dropzone', accepted, rejected);

		this.setState({uploadMessage: ''});
		let uploadMessage = 'Error:';
		if (rejected.length > 0) {
			rejected.forEach(oneRejected => {
				let file = oneRejected;
				if (file.size > this.attachmentsMaxSize) uploadMessage += ' File is too large.';
				if (this.attachmentsAllowedTypes.indexOf(file.type) < 0) uploadMessage += ' File type is incorrect.';
			});
			this.setState({uploadMessage: uploadMessage});
			return;
		}

		let attachmentCount = 0;
		accepted.forEach(oneAccepted => {

			let file = oneAccepted;
			let fr = new FileReader();

			this.setState({uploadMessage: 'Upload nr ' + ++attachmentCount + ': ' + file.name + '...'});

			fr.onload = (event) => {

				// Create attachment object and call the be
				let base64encodedAttachments = event.target.result;
				let attachment = {};
				attachment.name = file.name;
				attachment.content = base64encodedAttachments;

				logger.info('ATTACHMENT', attachment);
				
				this.axiosInstance.post('/messages/attachment', {
						attachment: attachment,
					})
					.then(response => {
						// Success callback
						logger.info('attachment upload response', response);

						if (response.data.status) {
							logger.info('data', response.data, this.state);
							this.setState({
								uploadMessage: 'Upload successful',
								uploaded: this.state.uploaded.indexOf(oneAccepted.name) == -1
											? this.state.uploaded.concat([response.data.name])
											: this.state.uploaded
							});
						}
					})
					.catch(response => {
						// Error callback
						logger.info('attachment upload error response', response);

						this.setState({uploadMessage: 'FAILED Upload of ' + file.name});
					});
			}
			fr.readAsDataURL(file);
		});
	}

	sendEmail() {

		logger.info('SENDEMAIL', this.state);

		// Validate
		if (!this.state.toAdmins && !this.state.toInstructors) {
			this.props.notify("You must select a 'To' group", {
				level: 'error'
			});
			return;
		}
		if (!this.state.subject && !this.state.content) {
			this.props.notify("You must fill in a subject and/or a content", {
				level: 'error'
			});
			return;
		}

		// Call be to send
		this.axiosInstance.post('/messages/send-email', {
			attachments: this.state.uploaded,
			content: this.state.content || '---',
			subject: this.state.subject,
			from: this.state.from,
			to: {
				toAdmins: this.state.toAdmins,
				toInstructors: this.state.toInstructors,
			}
		})
		.then(response => {
			logger.info('SENDEMAIL response', response);
			this.props.notify(response.data.message, {
				level: response.data.status ? 'success' : 'error'
			});

			if (response.data.rejected.length > 0) {
				this.props.notify('Emails failed for: ' + response.data.rejected.join(', '), {
					level: 'error',
					autoDismiss: 180,
				});
			}
		})
		.catch((response) => {
			logger.info('SENDEMAIL error', response.response);
			this.props.notify('Send Mail Error: ' + response.response.data, {
				level: 'error'
			});
		});

		this.props.notify("The emails are queued to be sent");
	}

	//
	// Renderers
	//
	render() {

		logger.info('RENDERING NOTIFY', this.state);

		const notificationChannels = () => {

			let title = 'Choose method...';
			let key='key';

			return (
				<DropdownHoverButton bsStyle="slbprimary" title={ title } /*key={ key }*/ id={ 'select-notification-channel' }>
					<MenuItem
						key='email'
						onSelect={ this.displayForm }>
						Email
					</MenuItem>
					<MenuItem
						disabled={ true }
						key='sms'
						onSelect={ this.displayVenue }>
						Text message (not available)
					</MenuItem>
				</DropdownHoverButton>
			);
		}

		const notificationForm = (
			<Form horizontal onChange={ this.handleFormChange }>

				<FormGroup controlId="from">
					<Col componentClass={ControlLabel} xs={3} sm={3} md={3} lg={3}>From</Col>
					<Col xs={9} sm={9} md={3} lg={3}>
						{ this.state.from }
					</Col>
				</FormGroup>

				<FormGroup controlId="toInstructors">
					<Col componentClass={ControlLabel} xs={3} sm={3} md={3} lg={3}>To</Col>
					<Col xs={9} sm={9} md={3} lg={3}>
						<Checkbox inline id='toInstructors' checked={ this.state.toInstructors }>Instructors</Checkbox>
					</Col>
				</FormGroup>

				<FormGroup controlId="toAdmins">
					<Col xs={12} xsOffset={3} smOffset={3} sm={9} md={3} mdOffset={3} lg={5} lgOffset={3}>
						<Checkbox inline id='toAdmins' checked={ this.state.toAdmins }>Admins</Checkbox>
					</Col>
				</FormGroup>

				<FormGroup controlId="subject">
					<Col componentClass={ControlLabel} xs={3} sm={3} md={3} lg={3}>Subject</Col>
					<Col xs={9} sm={9}><FormControl type="text" placeholder="Subject" /></Col>
				</FormGroup>

				<FormGroup controlId="content">
					<Col componentClass={ControlLabel} xs={3} sm={3} md={3} lg={3}>Email content</Col>
					<Col xs={9} sm={9}>
						<FormControl componentClass="textarea" placeholder="" />
					</Col>
				</FormGroup>

				<FormGroup controlId="attachments">
					<Col componentClass={ControlLabel} xs={3} sm={3} md={3} lg={3}>Attachments</Col>
					<Col xs={9} sm={9} md={9} lg={9}>
						<Dropzone name="attachments"
						          multiple={ true }
						          // accept={ this.attachmentsAllowedTypes }
						          maxSize={ this.attachmentsMaxSize }
						          style={{ width: '100%' }}
						          onDrop={ this.handleDropZone }>
							{({ isDragActive, isDragReject }) => {
								return 'Drop attachments HERE. ' +
									   // 'Must be of type .jpeg, .jpg, .png or .pdf. ' +
									   'Maximum size of all attachments together is 0.5 Mb';
							}}
						</Dropzone>
						<p style={{ color: 'red' }}>{ this.state.uploadMessage }</p>
						{ this.state.uploaded.length > 0 ?
							'Uploaded files: ' + this.state.uploaded.join(', ')
							: ''
						}
					</Col>
				</FormGroup>

				<Col gutterBottom collapseLeft xs={12} xsOffset={3} smOffset={3} sm={9} md={6} mdOffset={3} lg={5} lgOffset={3}>
					<ButtonGroup bsSize="sm">
						<Button bsStyle="slbneutral"  type="button" onClick={ this.sendEmail }><Icon glyph='icon-fontello-mail-1' /> Send Email</Button>
					</ButtonGroup>
				</Col>

			</Form>
		);
		
		return (
			<div>
				<PanelContainer>
					<Panel>
						<PanelBody style={{ padding: 0 }}>
							<Grid>
								<Row>
									<Col xs={12} sm={9} md={9} lg={6}>

										{ notificationChannels() }

										<p></p>

										{ notificationForm }

									</Col>
								</Row>
							</Grid>
						</PanelBody>
					</Panel>
				</PanelContainer>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return state;
}

Notify = connect (
	mapStateToProps
)(Notify)

export default Notify


