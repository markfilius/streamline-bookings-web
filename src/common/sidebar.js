// Third party imports
import React from 'react';
import { Link, withRouter, browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { createStore } from 'redux';
import {
  Sidebar, SidebarNav, SidebarNavItem,
  SidebarControls, SidebarControlBtn,
  LoremIpsum, Grid, Row, Col, FormControl, Image
} from '@sketchpixy/rubix';
let _ = require('lodash');

// Own imports
// import { env } from '../environment';
import * as Action from '../common/actions';
import * as logger from '../common/logger';


@withRouter
class CalendarSidebar extends React.Component {

	constructor(props) {
		super(props);
		logger.info('CALENDARSIDEBARCONST', props);
	}

	handleChange(e) {
		this._nav.search(e.target.value);
    }

    render() {
		return (
			<div>
				<Grid>
				    <Row>
					    <Col xs={12}>
							<div className='sidebar-nav-container'>
							    <SidebarNav style={{marginBottom: 0}} ref={(c) => this._nav = c}>

								    <div className='sidebar-header'>CALENDAR</div>

								    <SidebarNavItem glyph='icon-fontello-calendar-7' name='Edit' href='/i/calendar' {...this.props} ref={ 'defaultNavItem' }/>
								    {/*<SidebarNavItem glyph='icon-fontello-print'      name='Print' href='/i/calendar/print' />*/}
								    {/*<SidebarNavItem glyph='icon-dripicons-message'   name='Notifications' href='/i/calendar/notify' />*/}
								    {/*<SidebarNavItem glyph='icon-ikons-iphone'        name='Publish' href='/i/calendar/publish' />*/}
							    </SidebarNav>
							</div>
						{/*<FormControl type='text'*/}
			             {/*placeholder='Search for what ???'*/}
			             {/*onChange={::this.handleChange}*/}
			             {/*className='sidebar-search'*/}
			             {/*style={{border: 'none', background: 'none', margin: '10px 0 0 0', borderBottom: '1px solid #666', color: 'white'}} />*/}
					</Col>
				  </Row>
				</Grid>
			</div>
		);
  }
}

@withRouter
class StaffSidebar extends React.Component {

	constructor() {
		super();
	}

	componentDidMount() {}

	handleChange(e) {
		this._nav.search(e.target.value);
    }

    render() {
	  return (
	  <div>
		<Grid>
		  <Row>
			<Col xs={12}>
			  <div className='sidebar-nav-container'>
				<SidebarNav style={{marginBottom: 0}} ref={(c) => this._nav = c}>

				  <div className='sidebar-header'>STAFF</div>

  				  <SidebarNavItem glyph='icon-fontello-adult'      name='Edit' href='/i/staff' />
				  <SidebarNavItem glyph='icon-dripicons-message'   name='Notify All' href='/i/staff/notify' />
				</SidebarNav>
			  </div>
				{/*<FormControl type='text'*/}
				             {/*placeholder='Search for what ???'*/}
				             {/*onChange={::this.handleChange}*/}
				             {/*className='sidebar-search'*/}
				             {/*style={{border: 'none', background: 'none', margin: '10px 0 0 0', borderBottom: '1px solid #666', color: 'white'}} />*/}
			</Col>
		  </Row>
		</Grid>
	  </div>
	);
  }
}

@withRouter
class LevelsSidebar extends React.Component {

	constructor() {
		super();
	}

	componentDidMount() {}

	handleChange(e) {
		this._nav.search(e.target.value);
    }

    render() {
	  return (
	  <div>
		<Grid>
		  <Row>
			<Col xs={12}>
			  <div className='sidebar-nav-container'>
				<SidebarNav style={{marginBottom: 0}} ref={(c) => this._nav = c}>

				  <div className='sidebar-header'>LEVELS</div>

  				  <SidebarNavItem glyph='icon-fontello-adult'      name='Edit' href='/i/levels' />
				</SidebarNav>
			  </div>
				<FormControl type='text'
				             placeholder='Search for what ???'
				             onChange={::this.handleChange}
				             className='sidebar-search'
				             style={{border: 'none', background: 'none', margin: '10px 0 0 0', borderBottom: '1px solid #666', color: 'white'}} />
			</Col>
		  </Row>
		</Grid>
	  </div>
	);
  }
}

@withRouter
class GroupsSidebar extends React.Component {

	constructor() {
		super();
	}

	componentDidMount() {}

	handleChange(e) {
		this._nav.search(e.target.value);
	}

	render() {
		return (
			<div>
				<Grid>
					<Row>
						<Col xs={12}>
							<div className='sidebar-nav-container'>
								<SidebarNav style={{marginBottom: 0}} ref={(c) => this._nav = c}>

									<div className='sidebar-header'>GROUPS</div>

									<SidebarNavItem glyph='icon-fontello-users-3'    name='View & Edit' href='/i/groups' />
									{/*<SidebarNavItem glyph='icon-dripicons-message'   name='Notify' href='/i/groups/notify' />*/}
								</SidebarNav>
							</div>
							{/*<FormControl type='text'*/}
							             {/*placeholder='Search for what ???'*/}
							             {/*onChange={::this.handleChange}*/}
							             {/*className='sidebar-search'*/}
							             {/*style={{border: 'none', background: 'none', margin: '10px 0 0 0', borderBottom: '1px solid #666', color: 'white'}} />*/}
						</Col>
					</Row>
				</Grid>
			</div>
		);
	}
}

@withRouter
class SettingsSidebar extends React.Component {

	constructor() {
		super();
	}

	componentDidMount() {}

	handleChange(e) {
		this._nav.search(e.target.value);
    }

    render() {
	  return (
	  <div>
		<Grid>
		  <Row>
			<Col xs={12}>
			  <div className='sidebar-nav-container'>
				<SidebarNav style={{marginBottom: 0}} ref={(c) => this._nav = c}>

				  <div className='sidebar-header'>SETTINGS</div>

  				  <SidebarNavItem glyph='icon-fontello-swimming'   name='School' href='/i/settings/school' />
  				  <SidebarNavItem glyph='icon-fontello-warehouse'  name='Locations' href='/i/settings/locations' />
  				  <SidebarNavItem glyph='icon-fontello-calendar'   name='Terms & Rates' href='/i/settings/terms' />
				</SidebarNav>
			  </div>
				<FormControl type='text'
				             placeholder='Search for what ???'
				             onChange={::this.handleChange}
				             className='sidebar-search'
				             style={{border: 'none', background: 'none', margin: '10px 0 0 0', borderBottom: '1px solid #666', color: 'white'}} />
			</Col>
		  </Row>
		</Grid>
	  </div>
	);
  }
}

class DummySidebar extends React.Component {
  render() {
	return (
	  <Grid>
		<Row>
		  <Col xs={12}>
			<div className='sidebar-header'>DUMMY SIDEBAR</div>
			<LoremIpsum query='1p' />
		  </Col>
		</Row>
	  </Grid>
	);
  }
}

class ToggleMenu extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		/*
			This isn't working :-(
			On toggle I just want to set the state, not render the sidepanel. how?

		 */
		// this.setState({
		//     toggleMenuButtons: !this.state.toggleMenuButtons
		// });
		return (<div/>);
	}
}

@withRouter
class SidebarContainer extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			avatar: null,
			person: '',
			toggleMenuButtons: false,
			role: '',
			schoolName: 'Not logged in',
			venueName: this.props.venue ? this.props.venue.name : 'Not logged in',
		};
	}

	componentDidMount(props) {
		logger.info('SIDEBARDIDMOUNT', props);
	}

	componentDidUpdate(prevProps, prevState) {
	// componentWillReceiveProps(props) {
		logger.info('SIDEBARNEXTPROPS', prevProps, prevState);

		// this.setState({
		// 	avatar: null,
		// 	person: '',
		// 	role: '',
		// 	schoolName: 'Logged out',
		// 	venueName: 'Logged out',
		// });

		if (!this.props.venue) return;
		if (!this.props.person) return;

		let environment = this.props && this.props.environment;
		if (_.isEmpty(environment)) return;

		if ((!prevProps.venue || prevProps.venue.name !== this.props.venue.name) ||
			(!prevProps.person || prevProps.person.displayName !== this.props.person.displayName)) {

			this.setState({
				avatar: environment.imagesRootUrl + this.props.venue.logo,
				schoolName: this.props.school ? this.props.school.name : 'Error!',
				venueName: this.props.venue.name,
				person: this.props.person.displayName
			});

			if (this.props.venue.isAdmin) {
				this.setState({role: 'Administrator'});

			} else if (this.props.venue.isInstructor) {
				this.setState({role: 'Instructor'});
			}
		}
	}

	render() {
		return (
			<div id='sidebar'>
				<div id='avatar'>
				<Grid>
					<Row className='fg-white horizontal-center'>
						<Col xs={4} collapseRight>
							<div style={{ width: '150px', height: '75px' }}>
								<img src={ this.state.avatar }
									 style={{ height: '100%',
										      top: 0,
										      opacity: 0.5,
									          margin: 'auto',
										      display: this.state.avatar ? 'inherit' : 'none' }}/>
							</div>
							{/*<div width='200' height='50' style={{ display: this.state.avatar ? 'inherit' : 'none' }}>*/}
								{/*<Image src={ this.state.avatar } responsive />*/}
								{/*This doenst work nicely - wont scale into the parent :-( */}
							{/*</div>*/}
						</Col>
						<Col xs={8} collapseLeft id='avatar-col'>
							<div style={{top: 23, fontSize: 16, lineHeight: 1, position: 'relative'}}>{this.state.schoolName}</div>
							<div style={{top: 30, fontSize: 12, lineHeight: 1, position: 'relative'}}>{this.state.person}</div>
						</Col>
					</Row>
				</Grid>
			</div>
				<SidebarControls className={this.state.toggleMenuButtons ? 'hide' : ''}>
					<SidebarControlBtn bundle='fontello' glyph='calendar-7' sidebar={0}
					                   // onClick={ () => {
					                   // 	  console.log('0000000000');
					                   // 	  return (
						                //       <Sidebar sidebar={0}>
							            //           <CalendarSidebar {...this.props} selectie={'calendar'} />
						                //       </Sidebar>
					                   //    );
									   // }}
					                   />
					<SidebarControlBtn bundle='fontello' glyph='adult' sidebar={1} />
					<SidebarControlBtn bundle='fontello' glyph='chart-bar' sidebar={2} />
					<SidebarControlBtn bundle='fontello' glyph='certificate' sidebar={3} />
					<SidebarControlBtn bundle='fontello' glyph='users-3' sidebar={4} />
					<SidebarControlBtn bundle='fontello' glyph='tools' sidebar={5} />
					{/*<SidebarControlBtn bundle='ikons' glyph='more' sidebar={6} />  Remove for now */}
				</SidebarControls>
				<SidebarControls className={this.state.toggleMenuButtons ? '' : 'hide'}>
					<SidebarControlBtn bundle='fontello' glyph='plus' sidebar={0} />
				</SidebarControls>
				<div id='sidebar-container'>
					<Sidebar sidebar={0}>
						<CalendarSidebar {...this.props} selectie={'calendar'} />
					</Sidebar>
					<Sidebar sidebar={1}>
					    <StaffSidebar {...this.props} selectie={'staff'} />
					</Sidebar>
					<Sidebar sidebar={2}>
					   <LevelsSidebar {...this.props}/>
					</Sidebar>
					<Sidebar sidebar={3}>
					    <DummySidebar />
					</Sidebar>
					<Sidebar sidebar={4}>
					    <GroupsSidebar {...this.props} />
					</Sidebar>
					<Sidebar sidebar={5}>
					    <SettingsSidebar {...this.props} />
					</Sidebar>
					<Sidebar sidebar={6}>
					    <ToggleMenu {...this.props}/>
					</Sidebar>
				</div>
		  </div>
		);
  }
}

const mapStateToProps = state => {
	return state;
}

SidebarContainer = connect (
	mapStateToProps
)(SidebarContainer)

export default SidebarContainer

