// Third party imports
import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import {
	MenuItem,
	DropdownButton,
} from '@sketchpixy/rubix';

// Our imports
// import { env } from '../environment';
import * as logger from "./logger";


class StaffDropdown extends React.Component {

	constructor(props) {

		super(props);

		this.state = {
			staff: props.staff || [],
		}

		this.axiosInstance = null;
	}

	componentDidMount() {
		this.getStaff();
	}

	componentDidUpdate(prevProps, prevState) {
		logger.info('STAFFDROPDOWNDIDUPDATE', this.props);

		// Token is added from Redux store through props, possibly a short while after mounting
		if (prevProps.token !== this.props.token) this.getStaff();
	}

	getStaff() {

		let token = this.props && this.props.token;
		if (!token) return;

		let environment = this.props && this.props.environment;
		if (_.isEmpty(environment)) return;

		// Get my list of staff
		this.axiosInstance = axios.create({
			baseURL: environment.baseUri,
			headers: {token: token}
		});
		this.axiosInstance.post('/staff', {
			role: this.props.role,
		})
			.then(response => {
				this.setState({
					staff: response.data.staff
				});
			})
			.catch(response => {
				alert('Error ' + response.response.data);
			});
	}

	//
	// Renderers
	//
	render() {

		const staff = this.state.staff.map(staffMember => {
			return (
				<MenuItem
					key={ staffMember.id }
					eventKey={ staffMember.id }
					onSelect={ this.props.onSelect }
					className={ staffMember.isActive ? 'active-staff' : 'inactive-staff'}
					title={ staffMember.isActive ? 'This person is active' : 'This person is NOT active'}>
					{ staffMember.firstName + ' ' + staffMember.lastName }
				</MenuItem>
			);
		});

		let title = 'Choose Staff...';
		let key='key';
		const staffDropdown = (
			<DropdownButton bsStyle="slbprimary" title={title} key={key} id={'dropdown-basic'}>
				<MenuItem
					key='none'
					eventKey='none'
					onSelect={ this.props.onSelect }>
					(New)
				</MenuItem>
				<MenuItem divider />
				{ staff }
			</DropdownButton>
		);

		return (
			<div>
				{ staffDropdown }
			</div>
		);
	}
}

const mapStateToProps = state => {
	return state;
}

StaffDropdown = connect (
	mapStateToProps
)(StaffDropdown)

export default StaffDropdown


