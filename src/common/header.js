// Third party imports
import React from 'react';
import axios from 'axios';
import { browserHistory } from 'react-router';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { SidebarBtn, Navbar, Nav, NavItem, NavDropdown, NavDropdownHover, Icon, Grid, Row, Col, Badge, MenuItem, Entity, Label, Button } from '@sketchpixy/rubix';

let _ = require('lodash');
let moment = require('moment');

// Own imports
// import { env } from '../environment';
import * as Action from '../common/actions';
import * as logger from '../common/logger';


class Brand extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
	        venue: props.venue || {},
        };
    }

	componentWillReceiveProps(props) {
		if (props.venue) {
			this.setState({venue: props.venue});
		}
	}

	render() {
	    return (
            <Navbar.Header>
                <Navbar.Brand tabIndex='-1'>
                    <a href='#'>
                        { this.state.venue ? this.state.venue.name : '-' }
                    </a>
                </Navbar.Brand>
            </Navbar.Header>
        );
    }
}

class HeaderNavigation extends React.Component {
  render() {

      var props = {
        ...this.props,
        className: classNames('pull-right', this.props.className)
      };

      // These line require to avoid strange rubix/react error on props.handleLogout
	  var handleLogout = props ? props.handleLogout : null;
	  delete props.handleLogout;

      return (
          <Nav {...props}>
              <NavItem className='logout' href='#' onClick={ handleLogout }>
                  <Icon bundle='fontello' glyph='off-1' />
              </NavItem>
          </Nav>
      );
  }
}

class MessagesMenu extends React.Component {

	constructor(props) {
		super(props);
		logger.info('MESSAGESCONST', props);

		this.state = {
			person: props.person || {},
			venue: props.venue || {},
			token: props.token || null,
			messages: [],
		};

		this.handleReadMessage = this.handleReadMessage.bind(this);
	}

	componentDidMount(props) {
		logger.info('MESSAGESDIDMOUNT', props);
	}

	componentWillReceiveProps(props) {
		logger.info('MESSAGESNEXTPROPS', props);

		this.setState({
			person: props.person || {},
			venue: props.venue || {},
			token: props.token || null,
		});

		let environment = props && props.environment;
		if (_.isEmpty(environment)) return;

		// Get messages
		this.axiosInstance = axios.create({
			baseURL: environment.baseUri,
			headers: {token: props.token}
		});
		this.axiosInstance.post('/messages')
			.then(response => {
				logger.info('messages success response', response);
				this.setState({
					messages: response.data.messages || [],
				});
			})
			.catch((response) => {
				logger.info('messages error response', response.response, response);
			});
	}

	handleReadMessage = (event, message) => {

		event.preventDefault();

		logger.info('MESSAGEHANDLEREAD', message);
		this.axiosInstance.put('/messages/toggle', {
				message: message,
			})
			.then(response => {
				logger.info('message toggle success response', response);
				this.setState({
					messages: response.data.messages || [],
				});
				return false;
			})
			.catch((response) => {
				logger.info('message toggle error response', response.response, response);
			});

		return false;
	}

	render() {

		const messageIcon = () => {
			let newMessagesCount = this.state.messages.filter(message => message ? !message.isRead : false).length;
			return (
				<span>
			        <Icon bundle='fontello' glyph='mail-1'/>
			        <Badge className={ newMessagesCount ? 'have-new-messages' : 'have-no-messages'}>
				        { newMessagesCount }
			        </Badge>
				</span>
			)
		}

		const messagesList = () => this.state.messages.map(message => {
			if (!_.isEmpty(message)) {
				return (
					<MenuItem
						disabled
						key={message.id}
						title={message.title}>
						<Grid>
							<Row className={ message.isRead ? 'message-read' : 'message-unread' }>
								<Col xs={ message.isRead ? 10 : 12 } className='message-container' onClick={ (event) => this.handleReadMessage(event, message) }>
									<div className='message-time'>{ moment(message.datetime).fromNow() }</div>
									<div className='message-header'>{ message.title }</div>
									<div className='message-content'>{ message.content }</div>
								</Col>
								<Col xs={ message.isRead ? 2 : 0 } collapseLeft collapseRight>
									{ message.isRead
										? <Button onClick={(event) => this.handleReadMessage(event, message)}
										          bsClass={message.isRead ? 'btn-message-read' : 'btn-message-unread'}>{message.isRead ? 'Unread' : 'Read'}</Button>
										: ''
									}
								</Col>
							</Row>
						</Grid>
					</MenuItem>
				);
			}
		});

		return (
			<NavDropdownHover noCaret eventKey={6} title={ messageIcon() } id='notifications-menu' className='header-menu collapse-left'>
				<MenuItem header>Messages for { this.state.person ? this.state.person.displayName : 'Error, please contact support' }</MenuItem>
				{ messagesList() }
			</NavDropdownHover>
		);
	}
}

class DeletedMenu extends React.Component {

	constructor(props) {
		super(props);
		logger.info('DELETEDCONST', props);

		this.state = {
			person: props.person || {},
			venue: props.venue || {},
			token: props.token || null,
			deleted: [],
		};

		this.handleUnDelete = this.handleUnDelete.bind(this);
	}

	componentDidMount(props) {
		logger.info('DELETEDDIDMOUNT', props);
	}

	componentWillReceiveProps(props) {
		logger.info('DELETEDNEXTPROPS', props);

		this.setState({
			person: props.person || {},
			venue: props.venue || {},
			token: props.token || null,
		});

		let environment = props && props.environment;
		if (_.isEmpty(environment)) return;

		// Get deleted
		this.axiosInstance = axios.create({
			baseURL: environment.baseUri,
			headers: { token: props.token }
		});
		this.axiosInstance.post('/deleted')
			.then(response => {
				logger.info('deleted success response', response);
				this.setState({
					deleted: response.data.deleted || [],
				});
			})
			.catch((response) => {
				logger.info('deleted error response', response.response, response);
			});
	}

	handleUnDelete = (event, deleted) => {

		logger.info('UNDELETE', deleted);
		this.axiosInstance.put('/deleted/undelete', {
				deleted: deleted,
			})
			.then(response => {
				logger.info('undelete success response', response);

				this.setState({
					deleted: response.data.deleted || [],
				});

				this.props.notify(deleted.deletedWhat + ' is restored')

				// refresh page?
			})
			.catch((response) => {
				logger.info('undeleted error response', response.response, response);
			});
	}

	render() {

		const deletedIcon = () => {
			let newDeletedCount = this.state.deleted.filter(deleted => deleted ? !deleted.isRead : false).length;
			return (
				<span>
			        <Icon bundle='fontello' glyph='trash-1'/>
			        <Badge className={ 'have-no-messages' }>
				        { newDeletedCount }
			        </Badge>
				</span>
			)
		}

		const deletedList = () => this.state.deleted.map(deleted => {
			if (!_.isEmpty(deleted)) {
				return (
					<MenuItem
						disabled
						key={deleted.id}
						title={deleted.title}>
						<Grid>
							<Row className={ 'message-unread' }>
								<Col xs={ 10 } className='message-container' onClick={ (event) => this.handleUnDelete(event, deleted) }>
									<div className='message-time'>{ moment(deleted.deletedAt).fromNow() }</div>
									<div className='message-header'>{ deleted.deletedWhat }</div>
								</Col>
								<Col xs={ 2 } collapseLeft collapseRight>
                                    <Button onClick={(event) => this.handleUnDelete(event, deleted)}
								            bsClass={ 'btn-message-unread' }>{ 'Restore' }
						            </Button>
								</Col>
							</Row>
						</Grid>
					</MenuItem>
				);
			}
		});

		return (
			<NavDropdownHover noCaret eventKey={6} title={ deletedIcon() } id='notifications-menu' className='header-menu collapse-left'>
				<MenuItem header>Removed Items</MenuItem>
				{ deletedList() }
			</NavDropdownHover>
		);
	}
}

class Header extends React.Component {

    constructor(props) {
        super(props);
        logger.info('HEADERCONST', props);

        this.state = {
	        person: props.person ||null,
	        token: props.token || null,
	        venue: props.venue || null,
        }
    }

    componentWillReceiveProps(props) {
        logger.info('HEADERNEXTPROPS', props);
        if (props.venue) {
            this.setState({
	            venue: props.venue,
	            person: props.person,
	            token: props.token,
            });
        }
    }

    handleLogout() {

        logger.info('HANDLING Logout', this.state);

        this.setState({
            venue: null,
        });

        browserHistory.push({
            pathname: '/',
            state: {
                token: null,
                school: null,
                person: null,
	            venue: null,
            }
        });
    }

    //
    // Renderers
    //
    render() {

		logger.info('RENDERINGHEADER', this.state);

		return (
			<Grid id='navbar' >
				<Row>
					<Col xs={12}>
						<Navbar fixedTop fluid id='rubix-nav-header'>
							<Row>
								<Col xs={3} visible='xs'>
								    <SidebarBtn />
								</Col>
								<Col xs={6} sm={4}>
								    <Brand venue={ this.state.venue }/>
								</Col>
								<Navbar.Toggle />
								<Col xs={8}>
								    <Navbar.Collapse>
								        <Nav pullRight>
									        <MessagesMenu { ...this.state }/>
								            <DeletedMenu { ...this.state } { ...this.props }/>
								            <HeaderNavigation handleLogout={() => this.handleLogout()}/>
								        </Nav>
								    </Navbar.Collapse>
								</Col>
							</Row>
						</Navbar>
					</Col>
				</Row>
			</Grid>
		);
  }
}

const mapStateToProps = state => {
    return state;
}
const mapDispatchToProps = dispatch => {
	return {
		notify: (message, options) => dispatch(Action.notify(message, options)),
	}
}

Header = connect (
    mapStateToProps,
	mapDispatchToProps,
)(Header)

export default Header;


