
import { getEnvironment } from '../environment';

// let env = {
// 	loglevel: 'info'
// };

/*
 * logLevels:
 *      info
 *      debug
 *      warn
 *      error
 *      <anything else> e.g silent to remove all logging
 */


export function info() {

	let env = getEnvironment();
	if (!env) return;

	if (['info'].indexOf(env.logLevel) > -1) console.log('info', ...arguments);
}
export function debug() {

	let env = getEnvironment();
	if (!env) return;

	if (['info', 'debug'].indexOf(env.logLevel) > -1) console.log('debug', ...arguments);

}
export function warn() {

	let env = getEnvironment();
	if (!env) return;

	if (['info', 'debug', 'warn'].indexOf(env.logLevel) > -1) console.log('warn', ...arguments);

}
export function error() {

	let env = getEnvironment();
	if (!env) return;

	if (['info', 'debug', 'warn', 'error'].indexOf(env.logLevel) > -1) console.log('error', ...arguments);

}
