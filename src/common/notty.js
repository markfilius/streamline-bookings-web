/*
 * System Notifications to the browser to replace a js alert
 */

// Third party imports
import React from 'react';
import {connect} from 'react-redux';
import NotificationSystem from 'react-notification-system';

// Our imports
import {env} from '../environment';
import * as logger from '../common/logger';


class Notty extends React.Component {

	constructor(props) {

		super(props);
		logger.info('NOTTYCONST', props);

		this.state = {
			lastNotification: {},
			message: null,
			options: {},
		};

		this.notificationSystem = null;
	}

	componentDidMount(props) {
		logger.info('NOTTYDIDMOUNT', props, this.state);

		this.notificationSystem = this.refs.notificationSystem;
	}

	componentDidUpdate(prevProps, prevState) {

		let timenow = new Date().getTime();

		logger.info('NOTTYDIDUPDATE', timenow - this.state.lastNotification.timestamp, this.props);

		// If the message has changed or last update a while ago (>1s), add the notification
		if (prevProps.message !== this.props.message || (timenow - this.state.lastNotification.timestamp > 1000)) {
			this.addNotification(this.props.message, this.props.options);
		}
	}

	addNotification(message, options = {}) {
		// See: https://github.com/igorprado/react-notification-system

		if (!message) return;

		message = message.response || message;
		message = message.message || message.data || message.statusText || message;

		let messageText = Array.isArray(message) ? message.join('.  ') : (message.err || message);

		logger.info('Notification of:', messageText, options);

		if (!this.canDisplayNotification(messageText)) return;
		logger.info('Notification display allowed');

		const defaults = {
			message: messageText,
			level: 'success',       // success, error, warning and info
			position: 'bl',         // bottom left
			dismissable: true,
			autoDismiss: 3,
		};

		if (options.level === 'info') defaults.autoDismiss = 8;
		if (options.level === 'warning') defaults.autoDismiss = 8;
		if (options.level === 'error') defaults.autoDismiss = 15;

		let data = Object.assign({}, defaults, options);

		this.notificationSystem.addNotification(data);
	}

	// Return boolean to indicate if message can be displayed:
	//      no duplication of previous messages
	//      prevent empty messages
	canDisplayNotification(message) {

		// No empty messages
		if (!message || message === '' || message === '(Empty)') return false;

		let last = Object.assign({}, this.state.lastNotification);
		let timenow = new Date().getTime();
		this.setState({
			lastNotification: {
				timestamp: timenow,
				message: message,
			}
		});

		// Same message within 1.2 seconds? Then don't display
		if (last.timestamp && timenow - last.timestamp < 1200 && last.message === message) return false;
		return true;
	}


	//
	// Renderers
	//
	render() {

		logger.info('NOTTY RENDERING');

		return (
			<div>
				<NotificationSystem ref="notificationSystem"/>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return state;
}

Notty = connect(
	mapStateToProps
)(Notty)

export default Notty


