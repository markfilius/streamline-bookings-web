/*
 * Environment dependant variables
 */

let currentEnvironment = null;

export function getEnvironment (domain) {

	if (!domain) return currentEnvironment;

	let envData = {
		baseUri: 'http://localhost:9057/api/v1',
		imagesRootUrl: '/imgs/app/',
		invoicesRootUrl: '/invoices/',
		logLevel: 'debug',
		environment: null,
	};

	// Adjust for other environs
	switch (domain) {

		case 'localhost':
			envData.logLevel = 'debug';
			envData.environment = 'local';
			break;

		case 'bookings-dev.filius.cc':
			envData.baseUri = 'https://bookings-dev.filius.cc:9057/api/v1';
			envData.logLevel = 'debug';
			envData.environment = 'dev';
			break;

		case 'demo.streamlinebookings.com':
			envData.baseUri = 'https://demo.streamlinebookings.com/api/v1';
			envData.logLevel = 'warn';
			envData.environment = 'demo';
			break;

		case 'streamlinebookings.com':
			envData.baseUri = 'https://streamlinebookings.com/api/v1';
			envData.logLevel = 'warn';
			envData.environment = 'production';
			break;
	}
	
	currentEnvironment = envData;

	return envData;
}
