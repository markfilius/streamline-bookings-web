
// Third party imports
import React from 'react';
import { IndexRoute, Route, Router, Link, hashHistory, browserHistory  } from 'react-router';
import { syncHistoryWithStore  } from 'react-router-redux';
import { connect, Provider  } from 'react-redux';
import { createStore  } from 'redux';
import { Grid, Row, Col, MainContainer, Form, FormGroup, FormControl, ControlLabel, Button, HelpBlock  } from '@sketchpixy/rubix';
let _ = require('lodash');

// Own imports
import * as Action from './common/actions';
import Calendar from './routes/calendar';
import { getEnvironment } from './environment';
import Footer from './common/footer';
import Groups from './routes/groups';
import Header from './common/header';
import Levels from './routes/levels';
import Login from './routes/login';
import Sidebar from './common/sidebar';
import Settings from './routes/settings';
import Staff from './routes/staff';
import Notify from './common/notify';
import Notty from './common/notty';
import * as logger from './common/logger';

let store = createStore(Action.Slb);


export class App extends React.Component { 

    constructor(props) { 
        super(props);

        // Local state
        this.state = { 
            school: 'geen school',
            venue: 'geen locatie',
            person: 'geen persoon',
            token: 'geen token',
        };

	    // Log the initial state
	    logger.info('INITIALSTATE', store.getState(), this.props);

	    // When a notification arrives, update Notty
	    let unsubscribe = store.subscribe(() => {
	    	let newStore = store.getState();
		    logger.info('REDUXSTATECHANGEDTO', newStore.message, newStore );

		    // Not using state here to prevent unwanted notifications on other state changes
		    this.message = newStore.message;
		    this.options = newStore.options;
	    });
     }

	componentDidMount() {

    	// Set the environment data in redux
		// Must be done after mount (or window isn't available)
		store.dispatch(Action.setEnvironment(getEnvironment(window.document.domain)));

		// Refill the store with browser cached data
		if (localStorage.getItem('slb-school')) { 
			store.dispatch(Action.setSchool(JSON.parse(localStorage.getItem('slb-school'))));
		 }
		if (localStorage.getItem('slb-venue')) { 
			store.dispatch(Action.setVenue(JSON.parse(localStorage.getItem('slb-venue'))));
		 }
		if (localStorage.getItem('slb-person')) { 
			store.dispatch(Action.setPerson(JSON.parse(localStorage.getItem('slb-person'))));
		 }
		if (localStorage.getItem('slb-token')) {
			// logger.info('Setting Token from storage');
			store.dispatch(Action.setToken(localStorage.getItem('slb-token')));
		 }
	 }

	// componentWillReceiveProps(nextProps) {
	componentDidUpdate(prevProps, prevState) {

		logger.info('Route.js did update', this.props);

		// Expect this after login
		if (this.props.location.state && !_.isEqual(prevProps.location.state, this.props.location.state)) {

			// Set local state
			this.setState({ 
				school: this.props.location.state.school,
				venue: this.props.location.state.venue,
				person: this.props.location.state.person,
				token: this.props.location.state.token
			 });

			// Fill the redux store
			store.dispatch(Action.setSchool(this.props.location.state.school));
			store.dispatch(Action.setVenue(this.props.location.state.venue));
			store.dispatch(Action.setPerson(this.props.location.state.person));
			store.dispatch(Action.setToken(this.props.location.state.token));

			// Keep in browser storage
			localStorage.setItem('slb-school', JSON.stringify(this.props.location.state.school));
			localStorage.setItem('slb-venue', JSON.stringify(this.props.location.state.venue));
			localStorage.setItem('slb-person', JSON.stringify(this.props.location.state.person));
			localStorage.setItem('slb-token', this.props.location.state.token);
		 }
	 }

	render() { 
        return (
	        <Provider store={ store }>
		        <MainContainer { ...this.props  }>
			        <Notty message={ this.message } options={ this.options } />
			        <Sidebar { ...this.props  }/>
	                <Header { ...this.props  }/>
	                <div id='body'>
	                    <Grid>
	                        <Row>
	                            <Col xs={ 12 }>
	                                { this.props.children }
	                            </Col>
	                        </Row>
	                    </Grid>
	                </div>
	                <Footer />
	            </MainContainer>
	        </Provider>
        );
     }
 }

export default (
    <Router history={ hashHistory }>
        <Route path='/' component={ App }>
            <IndexRoute component={ Login }/>
            <Route path='/i'                      component={ App } />
            <Route path='/i/calendar'             component={ Calendar } />
	        <Route path='/i/groups'               component={ Groups } />
            <Route path='/i/levels'               component={ Levels } />
	        <Route path='/i/staff'                component={ Staff } />
	        <Route path='/i/staff/notify'         component={ Notify } detail='staff' />
            <Route path='/i/settings/school'      component={ Settings } detail='school' />
            <Route path='/i/settings/locations'   component={ Settings } detail='venues' />
            <Route path='/i/settings/terms'       component={ Settings } detail='terms' />
        </Route>
    </Router>
);
