// Third party imports
import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { CirclePicker } from 'react-color';
import {
	Row,
	Col,
	Nav,
	Tab,
	Icon,
	Grid,
	Clearfix,
	Panel,
	NavItem,
	MenuItem,
	PanelLeft,
	PanelBody,
	LoremIpsum,
	PanelRight,
	PanelHeader,
	PanelFooter,
	NavDropdown,
	PanelContainer,
	PanelTabContainer,
	DropdownHoverButton,
	Form,
	FormGroup,
	FormControl,
	ControlLabel,
	Checkbox,
	Button,
	InputGroup,
	ButtonGroup,
	ListGroup,
	ListGroupItem
} from '@sketchpixy/rubix';
let _ = require('lodash');

// Own imports
import * as Action from '../common/actions';
import debounceAxios from "../common/debounceAxios";
import LevelsDropdown from '../common/levelsDropdown';
import * as logger from '../common/logger';


class Levels extends React.Component {

	constructor(props) {

		super(props);
		logger.info('LEVELSCONST', props);

		this.state = {
			// environment: props.environment || {},
			// person: props.person ||null,
			// token: props.token || null,
			// venue: props.venue || null,
			levels: props.levels || [],
			level: props.level || {},
			proficiencies: props.proficiencies || [],
			proficiency: props.proficiency || {}
		}

		this.axiosInstance = null;

		// Bind 'this' to local functions
		this.handleChange = this.handleChange.bind(this);
		this.handleRemove = this.handleRemove.bind(this);
		this.handleColorChange = this.handleColorChange.bind(this);
		this.displayLevel = this.displayLevel.bind(this);
		this.handleProficiencyChange = this.handleProficiencyChange.bind(this);
		this.handleProficiencySubmit = this.handleProficiencySubmit.bind(this);
		this.handleProficiencyReorder = this.handleProficiencyReorder.bind(this);
		this.handleProficiencyRemove = this.handleProficiencyRemove.bind(this);

		// Get the levels. Possibly there isn't a token yet, so this call will be empty. That get's fixed in DidUpdate
		this.getLevels(props);
	}

	componentDidMount(props) {
		logger.info('LEVELSDIDMOUNT', props, this.state);

		// this.updateState(props);
		// this.getLevels(props);
		/*
		do we need this ?????
		this.setState({
			levels: this.props.levels,
		});
		*/
	}

	// componentWillReceiveProps(props) {
	// 	logger.info('LEVELSNEXTPROPS', props);
	//
	// 	this.updateState(props);
	// 	this.getLevels(props);
	// }

	componentDidUpdate(prevProps, prevState) {
		logger.info('LEVELSDIDUPDATE', prevProps, prevState);

		// Token is added from Redux store through props, possibly a short while after mounting
		if (prevProps.token !== this.props.token) this.getLevels(this.props);

		// After render, update the click events
		$('.proficiency-list').sortable({
			update: (event, ui) => {
				logger.info('AFTERSORTABLE', ui.item[0].id, $(ui.item[0]).prev().attr('id'));

				this.handleProficiencyReorder(ui.item[0].id, $(ui.item[0]).prev().attr('id'));
			}
		});
	}

	//
	// Level functions
	//
	getLevels(props) {
		logger.info('LEVELSGETTINGLEVELS', props, this.state);

		let token = props && props.token;
		let venue = props && props.venue;
		if (!token) return;

		let environment = props && props.environment;
		if (_.isEmpty(environment)) return;

		this.setState({
			levels: [],
		});

		// Get my list of levels
		this.axiosInstance = axios.create({
			baseURL: environment.baseUri,
			headers: {token: token}
		});
		this.axiosInstance.post('/levels')
			.then(response => {
				logger.info('levels success response', response);
				this.setState({
					levels: response.data.levels
				});
			})
			.catch((response) => {
				logger.info('levels error response', response.respone);
				this.props.notify(response, {level: 'error'});
			});
	}

	displayLevel (levelId, event) {
		logger.info('SELECTED', levelId, event);

		this.setState({
			level: {},
			proficiencies: [],
		})

		// Get the level's info
		this.axiosInstance.post('/level', {
			id: levelId
		})
			.then(response => {
				logger.info('single levels success response', response);

				this.setState({
					level: response.data.level,
					proficiencies: response.data.proficiencies,
					proficiency: {}
				});
			})
			.catch(response => {
				logger.info('single levels error response', response.response || response);

				let message = response.response
					? response.response.data.message
					: response;
				this.props.notify('Failed: ' + message, {level: 'error'});
			});
	}

	handleChange(event) {
		let newValue = {};
		newValue[event.target.id] = event.target.value;
		let debounceTimeout = event.target.id === 'notes' ? 4000 : 1500;
		let prevLevel = this.state.level;
		this.setState({level: Object.assign({}, prevLevel, newValue)}, () => {

			// Validation
			logger.info('VALIDATING+UPDATING', this.state.level);
			debounceAxios(
				this.axiosInstance,
				'put',
				'/level/update',
				{
					level: this.state.level
				},
				// Success callback
				response => {
					logger.info('validate+update level success response', response);
					if (response.data.errors) {
						this.props.notify(response, {
							level: (response.data.isValid ? 'warning' : 'error')
						});
					}

					// Update the selected level
					if (response.data.level) {
						this.setState({level: response.data.level});
					}

					// Update the dropdown
					this.getLevels();

					this.props.notify('Saved');
				},
				// Error callback
				response => {
					logger.info('validate+update level error response', response.response);
					this.props.notify(response, {level: 'error'});
				},
				debounceTimeout,
			);
		});
	}

	handleRemove(event) {
		logger.info('REMOVING', event, );

		event.preventDefault();

		this.axiosInstance.put('/level/delete', {
				level: this.state.level
			})
			.then(response => {
				logger.info('delete level success response', response);

				this.setState({
					level: {}
				});

				this.getLevels();
				this.displayLevel('none');

				this.props.notify('Level removed');
			})
			.catch(response => {
				logger.info('delete level error response', response, response.response);
				this.props.notify(response, {level: 'error'});
			});
	}

	handleColorChange(color, event) {
		if (!this.state.level.name) return;
		let target = {
			target: {
				id: 'color',
				value: color.hex
			}
		}
		this.handleChange(target);
	}

	//
	// Proficiency functions
	//
	handleProficiencyChange(event) {
		event.preventDefault();

		this.setState({
			proficiency: {
				name: event.target.value
			}
		});
	}


	handleProficiencySubmit(event) {
		if (event) event.preventDefault();

		if (!this.state.proficiency.name) return;

		debounceAxios(
			this.axiosInstance,
			'put',
			'/proficiency/update',
			{
				level: this.state.level,
				proficiency: this.state.proficiency
			},
			// Success callback
			response => {
				logger.info('submit proficiency success response', response);

				// Update state
				let newProficiencies = this.state.proficiencies.slice(0);   // create clone of all proficiencies
				newProficiencies.unshift(response.data.proficiency);        // add to the clone
				this.setState({
					proficiencies: newProficiencies,
					proficiency: {name: ''}
				});

				this.props.notify('Proficiency saved');
			},
			// Error callback
			response => {
				logger.info('submit proficiency error response', response);
				this.props.notify(response, {level: 'error'});
			},
			// Timeout for debounce
			300,
		);
	}

	handleProficiencyReorder(movingId, movedAfterId) {

		if (!movingId) return;

		this.setState({
			proficiencies: []
		})

		debounceAxios(
			this.axiosInstance,
			'put',
			'/proficiency/reorder',
			{
				level: this.state.level,
				movingId: movingId,
				movedAfterId: movedAfterId || null,
			},
			response => {
				logger.info('reorder proficiency success response', response);

				// Update state
				this.setState({
					proficiencies: response.data.proficiencies,
				});

				this.props.notify('Proficiency moved');
			},
			response => {
				logger.info('reorder proficiency error response', response);
				this.props.notify('Reordering proficiency failed: ' + response, {level: 'error'});
			},
			// Timeout for debounce
			300,
		);
	}

	handleProficiencyRemove(event, props) {
		logger.info('Prof rem', event, event.target.id, props);

		event.preventDefault();

		let proficiencyId = event.target.id;

		if (!proficiencyId) return;

		// Remove the proficiencies from the db
		this.axiosInstance.put('/proficiency/delete', {
			level: this.state.level,
			proficiencyId: proficiencyId
		})
			.then(response => {
				logger.info('delete prof success response', response);

				// Remove the proficiencies from state
				let newProficiencies = this.state.proficiencies.filter(prof => {
					return prof.id != proficiencyId;
				});
				this.setState({proficiencies: newProficiencies});

				this.props.notify('Proficiency removed');
			})
			.catch(response => {
				logger.info('delete prof error response', response);
			});


	}

	//
	// General functions
	//
	// updateState(props) {
	//
	// 	return;
	//
	// 	if (!props) return;
	//
	// 	this.setState((prev, props) => {
	//
	// 		let state = {
	// 			environment: props.environment,
	// 			person: props.person,
	// 			token: props.token,
	// 			venue: props.venue,
	// 		};
	// 		if (props.levels) state.levels = props.levels;
	//
	// 		return state;
	// 	});
	// }

	//
	// Renderers
	//
	render() {

		const levelForm = (
			<Form horizontal onChange={ this.handleChange }>
				<FormGroup controlId="name">
					<Col componentClass={ControlLabel} xs={2} sm={2} md={2} lg={1}>Name</Col>
					<Col xs={10} sm={10} md={10} lg={11}><FormControl type="text" placeholder="Level Name" value={this.state.level.name} /></Col>
				</FormGroup>

				<FormGroup controlId="cap">
					<Col componentClass={ControlLabel} xs={2} sm={2} md={2} lg={1}>Cap</Col>
					<Col xs={10} sm={10} md={10} lg={11}>
						<FormControl
							disabled={ this.state.level.isActive ? true : false }
							type="text"
							placeholder="Lesson Cap"
							value={this.state.level.cap}
						/>
						{ this.state.level.isActive
							? <span style="font-size:smallest;"><i>Cap is fixed when swimmers are enrolled at this level during the current term</i></span>
							: '' }
					</Col>
				</FormGroup>

				<FormGroup controlId="duration">
					<Col componentClass={ControlLabel} xs={2} sm={2} md={2} lg={1}>Duration</Col>
					<Col xs={10} sm={10} md={10} lg={11}><FormControl type="text" placeholder="Duration in minutes" value={this.state.level.duration}/></Col>
				</FormGroup>

				<FormGroup controlId="notes">
					<Col componentClass={ControlLabel} xs={2} sm={2} md={2} lg={1}>Notes</Col>
					<Col xs={10} sm={10} md={10} lg={11}><FormControl componentClass="textarea" rows="5" placeholder=""
					                                                  value={this.state.level.notes || ''}/></Col>
				</FormGroup>

				<FormGroup controlId="color">
					<Col componentClass={ControlLabel} xs={2} sm={2} md={2} lg={1}>Colour</Col>
					<Col xs={10} sm={10} md={10} lg={11}>
						<CirclePicker color={this.state.level.color} onChangeComplete={this.handleColorChange}
									  colors={[
							              "#ffffe0", "#ffe0ff", "#e0ffff", "#ff9999", "#9999ff", "#99ff99",
										  "#f44336", "#e91e63", "#9c27b0", "#673ab7", "#3f51b5", "#2196f3",
										  "#03a9f4", "#00bcd4", "#009688", "#4caf50", "#8bc34a", "#cddc39",
										  "#ffeb3b", "#ffc107", "#ff9800", "#ff5722", "#795548", "#607d8b"
									  ]}/>
					</Col>
				</FormGroup>


				<Col smOffset={2} lgOffset={1}>
					<ButtonGroup bsSize="sm">
						<Button bsStyle="slbneutral"
						        type="button"
						        onClick={this.handleRemove}>
							<Icon glyph='icon-fontello-trash-1' />
							Remove
						</Button>
					</ButtonGroup>
				</Col>
								
			</Form>

		);

		const proficiencies = () => {
			if (!this.state.proficiencies) {
				return '';
			}

			let proficiencyList = () => {

				let theProficiencies = this.state.proficiencies;

				let theList = theProficiencies.map(proficiency => {
					return (
						<div className='proficiency-pane' id={proficiency.id} key={proficiency.id} >
							{ proficiency.name }
							<Button className="slb-proficiency-pane-button"
							        bsStyle="slbsecondary"
							        id={ proficiency.id }
							        key={ proficiency.id }
							        onClick={ this.handleProficiencyRemove }>
								<Icon id={ proficiency.id } glyph='icon-fontello-minus' />
							</Button>
						</div>
					)
				});
				return theList;
			}

			return (
				<div>					
					<Row>
						<Col sm={12}>
							<Form onSubmit={this.handleProficiencySubmit} onChange={this.handleProficiencyChange}>
								<Col xs={12} sm={12} collapseLeft>
									<FormGroup controlId="name">
										<InputGroup>
											<FormControl type="text" placeholder="Add a proficiency" value={this.state.proficiency.name} />
											<InputGroup.Button>
												<Button bsStyle="slbsecondary" type="submit"><Icon glyph='icon-fontello-plus' /> Add</Button>
											</InputGroup.Button>
										</InputGroup>
									</FormGroup>
								</Col>
								<p></p>
							</Form>
						</Col>
					</Row>

					<div className='proficiency-list'>
						{ proficiencyList() }
					</div>
				</div>
			);
		}

		return (
			<div>
				<PanelContainer>
					<Panel>
						<PanelBody style={{ padding: 0 }}>
							<Grid>
								<Row>
									<Col xs={18} sm={12} md={6}>
										<Col xs={18} sm={12} smOffset={3} md={12} mdOffset={2}>
											<LevelsDropdown onSelect={ this.displayLevel }  levels={ this.state.levels }/>
											<p></p>
										</Col>
										<Col xs={18} sm={12} md={12}>
											{ levelForm }
											<p></p>
										</Col>
									</Col>								
									<Col xs={18} sm={12} md={6}>
										<Col xs={18} sm={12} md={12}>
											<h4>Proficiencies for { this.state.level.name }</h4>
										</Col>							
										<Col xs={18} sm={12} md={12}>
											{ proficiencies() }
										</Col>
									</Col>
								</Row>
							</Grid>
						</PanelBody>
					</Panel>
				</PanelContainer>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return state;
}
const mapDispatchToProps = dispatch => {
	return {
		notify: (message, options) => dispatch(Action.notify(message, options)),
	}
}

Levels = connect (
	mapStateToProps,
	mapDispatchToProps,
)(Levels)

export default Levels;


