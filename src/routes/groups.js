// Third party imports
import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import DatePicker from 'react-datepicker';
import { validate } from 'validate.js';
import {
	Row,
	Col,
	Clearfix,
	Tab,
	Tabs,
	Table,
	Icon,
	Grid,
	Panel,
	MenuItem,
	PanelBody,
	PanelContainer,
	DropdownButton,
	DropdownHoverButton,
	Form,
	FormGroup,
	FormControl,
	ControlLabel,
	Checkbox,
	Button,
	InputGroup,
	ButtonGroup,
	Label,
	Radio,
	Thumbnail,
} from '@sketchpixy/rubix';
let _ = require('lodash');
let moment = require('moment');

// Own imports
import * as Action from '../common/actions';
import debounceAxios from '../common/debounceAxios';
import LevelsDropdown from '../common/levelsDropdown';
import * as logger from '../common/logger';


class Groups extends React.Component {

	constructor(props) {

		super(props);
		logger.info('GROUPSCONST', props);

		this.state = {
			activeTab: 'group',
			group: props.group || {},
			groups: props.groups || [],
			groupPerson: props.groupPerson || {},
			groupPersons: props.groupPersons || [],
			groupDependant: props.groupDependant || {},
			groupDependants: props.groupDependants || [],
		}

		this.axiosInstance = null;

		// For some cases, provide a empty person. Just a {} doesn't work :-(
		this.emptyPerson = {
			title: '',
			firstName: '',
			lastName: '',
			phone: '',
			email: '',
			dob: '',
			gender: 'mx',
			relationship: '',
			isMainContact: false,
			isCarer: true,
			isSwimmer: false,
			isDependant: false,
			hasMedicalIndication: false,
			notes: '',
			level: null,
			levelsHistory: [],
		};

		// Bind 'this' to local functions
		this.handelGroupChange = this.handelGroupChange.bind(this);
		this.handleGroupRemove = this.handleGroupRemove.bind(this);
		this.displayGroup = this.displayGroup.bind(this);
		this.handelPersonChange = this.handelPersonChange.bind(this);
		this.handlePersonRemove = this.handlePersonRemove.bind(this);
		this.displayPerson = this.displayPerson.bind(this);
		this.setPassword = this.setPassword.bind(this);
		this.handelDependantChange = this.handelDependantChange.bind(this);
		this.handleDependantRemove = this.handleDependantRemove.bind(this);
		this.handleDobChange = this.handleDobChange.bind(this);
		this.handleNewLevel = this.handleNewLevel.bind(this);
		this.displayDependant = this.displayDependant.bind(this);
		this.handleSelectGroupsTab = this.handleSelectGroupsTab.bind(this);

		// Get the data. Possibly there isn't a token yet, so this call will be empty. That get's fixed in DidUpdate
		this.getGroups(props);
	}

	componentDidMount(props) {
		logger.info('GROUPSDIDMOUNT', props, this.state);
	}

	componentDidUpdate(prevProps, prevState) {
		// Token is added from Redux store through props, possibly a short while after mounting
		if (prevProps.token !== this.props.token) this.getGroups(this.props);

		/*
		do we need this ?????
		this.setState({
			group: this.props.group,
		});
		*/
	}

	//
	// Group functions
	//
	handelGroupChange(event) {
		let prevGroup = this.state.group;
		let newValue = {
			[event.target.id]: event.target.type === 'checkbox' ? event.target.checked : event.target.value,
		};
		logger.info('GROUPCHANGE', newValue, event.target, event.id, event);

		this.setState({group: Object.assign({}, prevGroup, newValue)}, () => {

			logger.info('GROUP UPDATING+VALIDATING', this.state.group);
			debounceAxios(
				this.axiosInstance,
				'put',
				'/group/update',
				{
					group: this.state.group,
				},
				// Success callback
				response => {
					logger.info('update group success response', response);
					if (response.data.errors) {
						this.props.notify(response.data.errors, {
							level: (response.data.isValid ? 'warning' : 'error')
						});
					}
					if (response.data.group) {
						this.setState({group: response.data.group});
					}

					this.getGroups();
					this.props.notify('Group saved');
				},
				// Error callback
				response => {
					logger.info('update groups error response', response);
					this.props.notify(response.response.data, {level: 'error'});
				},
			);
		});
	}

	handleGroupRemove(event) {
		event.preventDefault();

		this.axiosInstance.put('/group/delete', {
			group: this.state.group
		})
			.then(response => {
				logger.info('delete group success response', response);

				this.setState({
					group: {name: ''}
				});

				this.getGroups();
				this.displayGroup('none');
				this.props.notify('Group removed');
			})
			.catch(response => {
				logger.error('delete group error response', response);
				this.props.notify(response, {level: 'error'});
			});
	}

	handleSelectGroupsTab(key) {
		if (!key) return;
		this.setState({
			activeTab: key,
		});
	}

	getGroups(props) {
		logger.info('GROUPSGETTINGGROUPS', props);

		let token = props && props.token;
		if (!token) return;

		let environment = props && props.environment;
		if (_.isEmpty(environment)) return;

		// Get my list of groups
		this.axiosInstance = axios.create({
			baseURL: environment.baseUri,
			headers: {token: token}
		});
		this.axiosInstance.post('/groups')
			.then(response => {
				logger.info('groups success response', response);
				this.setState({
					groups: response.data.groups
				});
			})
			.catch(response => {
				logger.error('groups error response', response.response);
				this.props.notify(response.response.data, {level: 'error'});
			});
	}

	displayGroup(groupId, event) {
		logger.info('GROUPSELECTED', groupId, event, this.state);

		// Clear relevant state in case no info is returned
		this.setState({
			group: {},
			groupPerson: this.emptyPerson,
			groupPersons: [],
			groupDependant: this.emptyPerson,
			groupDependants: [],
		});

		// Get the group's info
		this.axiosInstance.post('/group', {
			id: groupId,
			venue: this.props.venue
		})
			.then(response => {
				logger.info('single group success response', response);

				this.setState({
					group: response.data.group,
					groupPersons: response.data.groupPersons,
					groupDependants: response.data.groupDependants,
					activeTab: 'group',
				});
			})
			.catch(response => {
				logger.error('single group error response', response);
				this.props.notify(response, {level: 'error'});

			});
	}

	//
	// Person functions (in this case, the persons are all Carers)(i.e. groupPerson should be called groupCarer)
	//
	handelPersonChange(event) {
		let prevPerson = this.state.groupPerson;
		let newValue = {
			[event.target.id]: event.target.type === 'checkbox' ? event.target.checked : event.target.value,
		};
		let eventTarget = event.target;
		// logger.info('UPDATEPERSONEVENTTARGET', eventTarget, eventTarget.id, eventTarget.value);

		this.setState({groupPerson: Object.assign({}, prevPerson, newValue)}, () => {

			// Don't save the passwords unless this action is initiated by the save btn
			if (this.state.groupPerson.setPassword && eventTarget.id !== 'password-save-btn') {
				// While we're here, check if password is valid
				let validateErrors = validate(this.state.groupPerson, {
					password: { length: {minimum: 8}},
					passwordConfirm: {equality: 'password'},
				});
				let newPerson = Object.assign({}, this.state.groupPerson, {passwordOk: validateErrors ? 'warning' : null});
				this.setState({
					groupPerson: newPerson
				});

				// Don't update
				return;
			}

			// Doing non-passwords? Hide the passwords
			if (this.state.groupPerson.setPassword && (eventTarget.id.indexOf('password') !== 0)) {
				this.setPassword(false);
			};

			logger.info('UPDATING+VALIDATING', this.state.groupPerson);
			debounceAxios(
				this.axiosInstance,
				'put',
				'/person/update',
				{
					group: this.state.group,
					groupPerson: this.state.groupPerson,
				},
				// Success callback
				response => {
					logger.info('update groupPerson success response', response);
					if (response.data.errors) {
						this.props.notify(response.data.errors, {
							level: (response.data.isValid ? 'warning' : 'error')
						});
					}

					// Ensure all password states are reset
					if (response.data.person) {
						response.data.person.setPassword = false;
						response.data.person.password = '';
						response.data.person.passwordConfirm = '';
					}

					// Replace the person in the group with the returned version, or just add
					let newGroupPersons = this.state.groupPersons ? this.state.groupPersons.slice(0) : [];  // create clone of all groupPersons
					let replaced = false;
					newGroupPersons = newGroupPersons.map(person => {
						if (person.id === response.data.person.id) {
							replaced = true;
							return response.data.person;
							return response.data.person;
						}
						return person;
					});
					if (!replaced) newGroupPersons.unshift(response.data.person);

					// Update state
					this.setState({
						groupPersons: newGroupPersons,
						groupPerson: response.data.person || {}
					});

					this.props.notify('Carer saved');
				},
				response => {
					logger.info('update groupPerson error response', response, response.response);
					this.props.notify(response, {level: 'error'});
				}
			);
		});
	}

	handlePersonRemove(event) {
		logger.info('REMOVINGPERSON', event);
		event.preventDefault();

		let removedId = this.state.groupPerson.id;

		this.axiosInstance.put('/person/delete', {
			group: this.state.group,
			groupPerson: this.state.groupPerson,
		})
			.then(response => {
				logger.info('delete groupPerson success response', response);

				// Update state
				let newGroupPersons = this.state.groupPersons.filter(person => person.id !== removedId);
				this.setState({
					groupPersons: newGroupPersons,
					groupPerson: {
						firstName: '',
						lastName: ''
					}
				});

				this.props.notify('Person removed');
			})
			.catch(response => {
				logger.error('delete groupPerson error response', response);
				this.props.notify(response, {level: 'error'});
			});
	}

	displayPerson(personId, event) {
		logger.info('PERSONSELECTED', personId, this.state);

		let person = this.state.groupPersons.filter(person => person.id === personId);

		// Merge the person with empty person to fill any default values and override existing person values
		person = Object.assign({}, this.emptyPerson, person[0]);

		this.setState({
			groupPerson: person,
			activeTab: 'person',
		});
	}

	setPassword (showSetPassword) {

		let newStatus = typeof showSetPassword === 'boolean' ? showSetPassword : !this.state.groupPerson.setPassword;

		this.setState({groupPerson: Object.assign({}, this.state.groupPerson, {
			setPassword: newStatus
		})});
	}

	//
	// Dependant functions
	//
	handelDependantChange(event) {
		let prevDependant = this.state.groupDependant;
		// let newValue = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
		let newValue ;
		if (event.target.type === 'checkbox') {
			newValue = event.target.checked;
		} else {
			switch (event.target.value) {
				case 'true': newValue = true; break;
				case 'false': newValue = false; break;
				default:
					newValue = event.target.value;
			}
		}
		let newValues = {
			[event.target.id]: newValue,
			isDependant: true,
			isCarer: false,
		};
		let eventTarget = event.target;
		// logger.info('UPDATEDEPENDANTEVENTTARGET', eventTarget, eventTarget.id, eventTarget.value);

		this.setState({groupDependant: Object.assign({}, prevDependant, newValues)}, () => {

			logger.info('DEPENDANT UPDATING+VALIDATING', this.state.groupDependant);

			let timeout = eventTarget.id === 'newLevel' ? 500 : null; // null => default

				debounceAxios(
				this.axiosInstance,
				'put',
				'/person/update',
				{
					group: this.state.group,
					groupPerson: this.state.groupDependant,
				},
				// Success callback
				response => {
					logger.info('update groupDependant success response', response);
					if (response.data.errors) {
						this.props.notify(response.data.errors, {
							level: (response.data.isValid ? 'warning' : 'error')
						});
					}

					// Replace the dependant in the group with the returned version, or just add
					let newGroupDependants = this.state.groupDependants ? this.state.groupDependants.slice(0) : [];  // create clone of all groupDependants
					let replaced = false;
					newGroupDependants = newGroupDependants.map(dependant => {
						if (dependant.id === response.data.person.id) {
							replaced = true;
							return response.data.person;
						}
						return dependant;
					});
					if (!replaced) newGroupDependants.unshift(response.data.person);

					// Update state
					this.setState({
						groupDependants: newGroupDependants,
						groupDependant: response.data.person
					});

					this.props.notify('Dependant saved');
				},
				// Error callback
				response => {
					logger.info('update groupDependants error response', response, response.response);
					this.props.notify(response, {level: 'error'});
				},
				timeout,
			);
		});
	}

	handleDependantRemove(event) {
		logger.info('REMOVINGDEPENDANT', event);
		event.preventDefault();

		let removedId = this.state.groupDependant.id;

		this.axiosInstance.put('/person/delete', {
			group: this.state.group,
			groupPerson: this.state.groupDependant,
		})
			.then(response => {
				logger.info('delete groupDependant success response', response);

				// Update state
				let newGroupDependants = this.state.groupDependants.filter(dependant => dependant.id !== removedId);
				this.setState({
					groupDependants: newGroupDependants,
					groupDependant: {
						firstName: '',
						lastName: ''
					}
				});

				this.props.notify('Dependant removed');
			})
			.catch(response => {
				logger.error('delete groupDependant error response', response);
				this.props.notify(response, {level: 'error'});
			});
	}

	displayDependant(dependantId, event) {
		logger.info('DEPENDANTSELECTED', dependantId, this.state);

		let dependant = this.state.groupDependants.filter(dependant => dependant.id === dependantId);

		// Merge the dependant with empty dependant to fill any default values and override existing dependant values
		dependant = Object.assign({}, this.emptyPerson, dependant[0]);

		this.setState({
			groupDependant: dependant,
			activeTab: 'dependant',
		});
	}

	handleDobChange = (dob) => {
		logger.info('GROUPHANDLECHANGEDATE', dob);

		// Create fake event and call usual event handler
		let event = {
			target: {
				id: 'dob',
				value: dob,
				type: 'fake'
			}
		};

		this.handelDependantChange(event);
	}

	handleNewLevel(levelId) {
		logger.info('GROUPHANDLENEWLEVEL', levelId);

		if (!levelId) return;

		// Create fake event and call usual event handler
		let event = {
			target: {
				id: 'newLevel',
				value: levelId,
				type: 'fake'
			}
		};

		this.handelDependantChange(event);
	}

	//
	// Renderers
	//
	render() {

		const groupForm = (
			<Form horizontal onChange={this.handelGroupChange}>

				<FormGroup controlId="name">
					<Col componentClass={ControlLabel} xs={2} sm={2} md={2} lg={1}>Name<sup>*</sup></Col>
					<Col xs={10} sm={10} md={10} lg={11}><FormControl type="text" placeholder="Family/Group name" value={this.state.group.name} /></Col>
				</FormGroup>

				<FormGroup controlId="address">
					<Col componentClass={ControlLabel} xs={2} sm={2} md={2} lg={1}>Address</Col>
					<Col xs={10} sm={10} md={10} lg={11}><FormControl type="text" placeholder="Address" value={this.state.group.address}/></Col>
				</FormGroup>

				<FormGroup controlId="suburb">
					<Col componentClass={ControlLabel} xs={2} sm={2} md={2} lg={1}>Suburb</Col>
					<Col xs={10} sm={10} md={10} lg={11}><FormControl type="text" placeholder="Suburb" value={this.state.group.suburb}/></Col>
				</FormGroup>

				<FormGroup>
					<Col componentClass={ControlLabel} xs={2} sm={2} md={2} lg={1}>State</Col>
					<Col xs={4} sm={4} md={4} lg={5}>
						<FormControl id="state" type="text" placeholder="State" value={this.state.group.state} />
					</Col>
					<Col xs={6} sm={6} md={6} lg={6}>
						<FormControl id="postcode" type="text" placeholder="Postcode" value={this.state.group.postcode} />
					</Col>

				</FormGroup>

				{/*Email is not used (yet) for a group*/}
				{/*<FormGroup controlId="email">*/}
					{/*<Col componentClass={ControlLabel} xs={2} sm={2} md={2} lg={1}>Email</Col>*/}
					{/*<Col xs={10} sm={10} md={10} lg={11}>*/}
					{/*<FormControl type="email" plaupaceholder="Email" value={this.state.group.email}/>*/}
					{/*</Col>*/}
				{/*</FormGroup>*/}

				<FormGroup>
					<Col xs={5} xsOffset={2} sm={5} smOffset={2} md={3} mdOffset={2} lg={3} lgOffset={1}>
						<Checkbox inline id="isPaused" checked={this.state.group.isPaused}>Pause this group</Checkbox>
					</Col>
				</FormGroup>

				{/*The active status wil be used at a later date*/}
				{/*<FormGroup controlId="isInactive" className={this.state.group.active ? "hide" : ""}>*/}
					{/*<Col smOffset={2}>*/}
						{/*<p>This group {this.state.group.name} is not active</p>*/}
					{/*</Col>*/}
				{/*</FormGroup>*/}

				<FormGroup controlId="notes">
					<Col componentClass={ControlLabel} xs={2} sm={2} md={2} lg={1}>Notes</Col>
					<Col xs={10} sm={10} md={10} lg={11}><FormControl rows='5' componentClass="textarea" placeholder="" value={this.state.group.notes}/></Col>
				</FormGroup>

				<Col xs={10} xsOffset={2} sm={10} smOffset={2} md={10} mdOffset={2} lg={11} lgOffset={1} collapseLeft>
					<ButtonGroup bsSize="sm">
						<Button bsStyle="slbneutral" type="button" onClick={this.handleGroupRemove}><Icon glyph='icon-fontello-trash-1' /> Remove</Button>
					</ButtonGroup>
				</Col>

			</Form>
		);

		const groupPersonForm = () => {
			return (
				<Form horizontal onChange={this.handelPersonChange}>

				<FormGroup>
					<Col componentClass={ControlLabel} xs={2} sm={2} md={2} lg={1}>Name<sup>*</sup></Col>
					<Col xs={2} sm={2} md={2} lg={1} collapseRight><FormControl id="title" type="text"
					                                                            placeholder="Title"
					                                                            value={this.state.groupPerson.title || ''}/></Col>
					<Col xs={4} sm={4} md={4} lg={5} collapseRight><FormControl id="firstName" type="text"
					                                                            placeholder="First Name"
					                                                            value={this.state.groupPerson.firstName || ''}/></Col>
					<Col xs={4} sm={4} md={4} lg={5} collapseLeft><FormControl id="lastName" type="text"
					                                                           placeholder="Last Name"
					                                                           value={this.state.groupPerson.lastName || ''}/></Col>
				</FormGroup>

				{/* For now: don't show this
				<FormGroup controlId="useGroupAddress">
					<Col xs={10} xsOffset={2} sm={10} smOffset={2} md={10} mdOffset={2} lg={11} lgOffset={1}>
						<Checkbox inline id='useGroupAddress' checked={true} disabled>Use group address</Checkbox>
					</Col>
				</FormGroup>
				*/}

				<FormGroup controlId="phone">
					<Col componentClass={ControlLabel} xs={2} sm={2} md={2} lg={1}>Phone</Col>
					<Col xs={10} sm={10} md={10} lg={11}><FormControl type="phone" placeholder="Phone"
					                                               value={ this.state.groupPerson.phone || '' }/></Col>
				</FormGroup>

				<FormGroup controlId="email">
					<Col componentClass={ControlLabel} xs={2} sm={2} md={2} lg={1}>Email<sup>*</sup></Col>
					<Col xs={8} sm={8} md={8} lg={9}><FormControl type="email" placeholder="Email (required for carers)"
					                                               value={ this.state.groupPerson.email || '' }/></Col>
					<Col xs={2} sm={2} md={2} lg={1} collapseLeft><Button onClick={ this.setPassword }>Set Password</Button></Col>
				</FormGroup>

				<FormGroup className={ this.state.groupPerson.setPassword ? "" : "hide"} validationState={ this.state.groupPerson.passwordOk }>
					<Col componentClass={ControlLabel} xs={2} sm={2} md={2} lg={1}>Password</Col>
					<Col xs={5} sm={5} md={4} lg={4}><FormControl id="password" type="password" value={ this.state.groupPerson.password || '' } placeholder="New once only password, 8+ chars"/></Col>
					<Col xs={5} sm={5} md={4} lg={4} collapseLeft><FormControl id="passwordConfirm" type="password" value={ this.state.groupPerson.passwordConfirm || '' } placeholder="Retype the password"/></Col>
					<Col xs={5} sm={5} md={2} lg={2} collapseLeft><Button id="password-save-btn" onClick={ this.handelPersonChange }>Save</Button></Col>
				</FormGroup>

				<FormGroup controlId="relationship">
					<Col componentClass={ControlLabel} xs={2} sm={2} md={2} lg={1}>Relationship</Col>
					<Col xs={10} sm={10} md={10} lg={11}><FormControl type="relationship" placeholder="To dependants"
					                                               value={this.state.groupPerson.relationship || ''}/></Col>
				</FormGroup>

				<FormGroup>
					<Col componentClass={ControlLabel} xs={2} sm={2} md={2} lg={1}>This person is a</Col>
					<Col xs={2} sm={2} md={2} lg={2} rightCollapse>
						<Checkbox inline id='isMainContact' checked={ this.state.groupPerson.isMainContact}>
							main contact</Checkbox>
					</Col>

					<Col xs={2} sm={2} md={2} lg={2} leftCollapse rightCollapse>
						<Checkbox inline id='isCarer' checked={this.state.groupPerson.isCarer}>
							carer</Checkbox>
					</Col>

					<Col xs={2} sm={2} md={2} lg={2} leftCollapse rightCollapse>
						<Checkbox inline id='isSwimmer' checked={this.state.groupPerson.isSwimmer}>swimmer</Checkbox>
					</Col>

					<Col xs={3} sm={3} md={3} lg={2} leftCollapse rightCollapse>
						<Checkbox inline id="isActiveMobile" checked={this.state.groupPerson.isActiveMobile} disabled>mobile app user</Checkbox>
					</Col>
				</FormGroup>

				<FormGroup controlId="notes">
					<Col componentClass={ControlLabel} xs={2} sm={2} md={2} lg={1}>Notes</Col>
					<Col xs={10} sm={10} md={10} lg={11}><FormControl rows='5' componentClass="textarea" placeholder=""
					                                                  value={this.state.groupPerson.notes || ''}/></Col>
				</FormGroup>

				<Col xs={10} xsOffset={2} sm={10} smOffset={2} md={10} mdOffset={2} lg={11} lgOffset={1} collapseLeft>
					<ButtonGroup bsSize="sm">
						<Button bsStyle="slbneutral" type="button" onClick={this.handlePersonRemove}><Icon
							glyph='icon-fontello-trash-1'/> Remove</Button>
					</ButtonGroup>
				</Col>

			</Form>
			)
		};

		const groupDependantForm = () => {

			const levelsHistory = () => {

				if (!this.state.groupDependant.levelsHistory || this.state.groupDependant.levelsHistory.length <= 0) return;

				let key='keyLevelsHistory';
				let title = 'Show History...';

				const allLevels = this.state.groupDependant.levelsHistory.map(level => {
					return (
						<MenuItem
							key={ level.id + Math.floor(Math.random() * 99999).toString()}      // Ensure a unique key
							eventKey={ level.id }>
							{ level.name + (level.startDate ? ', ' + moment(level.startDate).format('Do MMM') : '')}
						</MenuItem>
					);
				});

				return (
					<DropdownButton bsStyle="slbprimary" title={title} key={key} id={'dropdown-basic'}>
						{ allLevels }
					</DropdownButton>
				)
			}

			const currentLevel = () => {
				if (this.state.groupDependant.atLevels && this.state.groupDependant.atLevels[0] && this.state.groupDependant.atLevels[0].name) {
					return (
						<div>
							<span style={{
								'width': '10px',
								'backgroundColor': this.state.groupDependant.atLevels[0].color
							}}>_</span>
							<span>{ ' ' +
									this.state.groupDependant.atLevels[0].name +
									(this.state.groupDependant.atLevels[0].startDate
										? ', ' + moment(this.state.groupDependant.atLevels[0].startDate).format('D MMM')
										: ''
									)
							}</span>
						</div>
					)
				}
				return ('-');
			}

			return (
				<Form horizontal onChange={this.handelDependantChange}>

					<FormGroup>
						<Col componentClass={ControlLabel} xs={2} sm={2} md={2} lg={1}>Name</Col>
						<Col xs={5} sm={5} md={5} lg={5} collapseRight><FormControl id="firstName" type="text"
						                                                            placeholder="First Name"
						                                                            value={ this.state.groupDependant.firstName}/></Col>
						<Col xs={5} sm={5} md={5} lg={6} collapseLeft><FormControl id="lastName" type="text"
						                                                           placeholder="Last Name"
						                                                           value={ this.state.groupDependant.lastName}/></Col>
					</FormGroup>

					{/* For now: don't show this
					<FormGroup controlId="useGroupAddress">
						<Col xs={10} xsOffset={2} sm={10} smOffset={2} md={10} mdOffset={2} lg={11} lgOffset={1}>
							<Checkbox inline id='useGroupAddress' checked={true} disabled>Use group address</Checkbox>
						</Col>
					</FormGroup>
					*/}

					<FormGroup controlId="phone">
						<Col componentClass={ControlLabel} xs={2} sm={2} md={2} lg={1}>Phone</Col>
						<Col xs={10} sm={10} md={10} lg={11}><FormControl type="phone" placeholder="Phone"
						                                               value={this.state.groupDependant.phone}/></Col>
					</FormGroup>

					<FormGroup controlId="email">
						<Col componentClass={ControlLabel} xs={2} sm={2} md={2} lg={1}>Email</Col>
						<Col xs={10} sm={10} md={10} lg={11}><FormControl type="email" placeholder="Email"
						                                               value={this.state.groupDependant.email}/></Col>
					</FormGroup>

					<FormGroup controlId="dob">
						<Col componentClass={ControlLabel} xs={2} sm={2} md={2} lg={1}>Date Of Birth</Col>
						<Col xs={10} sm={10} md={10} lg={11}>
							<DatePicker id="dob"
							            onChange={ this.handleDobChange }
										selected={ this.state.groupDependant.dob ? moment(this.state.groupDependant.dob) : '' }
										openToDate={ this.state.groupDependant.dob ? moment(this.state.groupDependant.dob) : moment().subtract(5, 'years') }
								        dateFormat="DD-MM-YYYY"
								        showYearDropdown={true}
								        placeholderText="Date Of Birth"/>
						</Col>
					</FormGroup>

					<FormGroup controlId="hasMedicalIndication">
						<Col componentClass={ControlLabel} xs={2} sm={2} md={2} lg={1}>Medical Indication</Col>
						<Col xs={2} sm={2} md={2} lg={1}>
							<Radio inline id='hasMedicalIndication' value={ true }
							       checked={this.state.groupDependant.hasMedicalIndication}>Yes</Radio>
							<Radio inline id='hasMedicalIndication' value={ false }
							       checked={!this.state.groupDependant.hasMedicalIndication}>No</Radio>
						</Col>
						<Col xs={8} sm={8} md={8} lg={10}>
							{this.state.groupDependant.hasMedicalIndication ?
								<FormControl id="medicalIndication"
								             type="text"
								             placeholder="Short description"
								             value={this.state.groupDependant.medicalIndication}/>
								: null
							}
						</Col>
					</FormGroup>

					<FormGroup controlId="gender">
						<Col componentClass={ControlLabel} xs={2} sm={2} md={2} lg={1}>Gender</Col>
						<Col xs={10} sm={10} md={10} lg={11}>
							<Radio inline id='gender' value='mr'
							       checked={this.state.groupDependant.gender === 'mr'}>Male</Radio>
							<Radio inline id='gender' value='ms'
							       checked={this.state.groupDependant.gender === 'ms'}>Female</Radio>
							<Radio inline id='gender' value='mx' checked={this.state.groupDependant.gender === 'mx'}>Not
								specified</Radio>
						</Col>
					</FormGroup>

					<FormGroup controlId="level">
						<Col componentClass={ControlLabel} xs={2} sm={2} md={2} lg={1}>Level</Col>
						<Col xs={3} sm={3} md={2} lg={3}>
							{ currentLevel() }
						</Col>
						<Col xs={3} sm={3} md={2} lg={3} collapseLeft>
							<LevelsDropdown title={'Choose new level...'} onSelect={this.handleNewLevel}/>
						</Col>
						<Col xs={4} sm={4} md={2} lg={3}>
							{ levelsHistory() }
						</Col>
					</FormGroup>

					<FormGroup controlId="notes">
						<Col componentClass={ControlLabel} xs={2} sm={2} md={2} lg={1}>Notes</Col>
						<Col xs={10} sm={10} md={10} lg={11}><FormControl componentClass="textarea" rows="5" placeholder=""
						                                               value={this.state.groupDependant.notes}/></Col>
					</FormGroup>

					<Col xs={10} xsOffset={2} sm={10} smOffset={2} md={10} mdOffset={2} lg={11} lgOffset={1} collapseLeft>
						<ButtonGroup bsSize="sm">
							<Button bsStyle="slbneutral" type="button" onClick={this.handleDependantRemove}><Icon
								glyph='icon-fontello-trash-1'/> Remove</Button>
						</ButtonGroup>
					</Col>

				</Form>
			)
		};

		const financialsForm = () => {

			let momentDatetimeFormat = 'Do MMM, h:mma';
			let momentDatetimeYearFormat = 'Do MMM YYYY, h:mma';

			return (
				<Table striped bordered condensed hover responsive>
					<thead>
						<tr>
							<th>Payment made</th>
							<th>By</th>
							<th>Amount</th>
							<th>Description</th>
							<th>Payment method</th>
							<th>Invoice</th>
						</tr>
					</thead>
					<tbody>
						{ this.state.group.payments && this.state.group.payments.map(payment => {
							return (
								<tr key={ payment.id }>
									<td>{ moment(payment.datetime).format(momentDatetimeYearFormat) }</td>
									<td>{ payment.payers.length > 0 && payment.payers[0].firstName
											? payment.payers[0].firstName + ' ' + payment.payers[0].lastName
											: '?'
									}</td>
									<td>{ '$ ' + (payment.amount / 100).toFixed(2) }</td>
									<td>{
										(payment.swimmers.length > 0
											? payment.swimmers[0].firstName + ' ' + payment.swimmers[0].lastName
											: 'name?') + ', ' +
										(payment.classes.length > 0 && payment.classes[0].level
											? payment.classes[0].level.name || 'class?'
											: 'class?') + ', ' +
										(payment.classes.length > 0
											? moment(payment.classes[0].datetime).format(momentDatetimeFormat)
											: 'date/time?')
									}</td>
									<td>{ payment.paymentMethods.length > 0
											? payment.paymentMethods[0].type + ' ****' + payment.paymentMethods[0].last4
											: '-'
									}</td>
									<td>
										{ payment.invoiceFileName ?
											<a href={ this.props.environment.invoicesRootUrl + payment.invoiceFileName } target={ '_blank' }>View</a>
											: null }
									</td>
								</tr>
							);
						})}
					</tbody>
				</Table>
			)
		};


		const groups = () => {

			let listOfIds = [];

			return this.state.groups.map(group => {
				if (listOfIds.indexOf(group.id) != -1) logger.info('DUPLICATEKEY IN, FOR: ', listOfIds, group);
				listOfIds.push(group.id);
				return (
					<MenuItem
						key={group.id}
						eventKey={group.id}
						onSelect={ this.displayGroup }
						className={ group.isActive ? 'active-group' : 'inactive-group' }
						title={ group.isActive ? 'This group is active' : 'This group is NOT active'}>
						{ group.name + (group.suburb ? ', ' + group.suburb : '') }
					</MenuItem>
				);
			});
		}

		let title = 'Family/Group...';
		let key = 'key';
		const groupsDropdown = (
			<div>
				<Col lg={2} md={3} sm={3} xs={4} collapseLeft>
					<DropdownHoverButton bsStyle="slbprimary" title={ title } key={ key } id={ 'dropdown-basic' }>
						<MenuItem
							key='none'
							eventKey='none'
							onSelect={ this.displayGroup }>
							(None)
						</MenuItem>
						<MenuItem divider/>
						{ groups() }
					</DropdownHoverButton>
				</Col>
			</div>
		);

		const groupPersons = () => {

			if (!this.state.groupPersons) return (<div/>);

			let listOfIds = [];

			return this.state.groupPersons.map(person => {
				listOfIds.push(person.id);
				return (
					<MenuItem
						key={person.id}
						eventKey={person.id}
						onSelect={ this.displayPerson }
						className={ person.active ? 'active-person' : 'inactive-person' }
						title={ person.active ? 'This person is active' : 'This person is NOT active'}>
						{ person.firstName + ' ' + person.lastName }
						<Label bsStyle={ person.isCarer ? 'default' : 'warning' }>{ person.isCarer ? '' : 'NOTCarer'}</Label>
					</MenuItem>
				);
			});
		}

		const groupPersonsDropdown = () => {

			let title = 'Carer...';
			let key = 'key';
			if (!this.state.groupPersons || this.state.groupPersons.length <= 0) return (<div/>);
			return (
				<div>
					<Col lg={2} md={3} sm={3} xs={4}>
						<DropdownHoverButton bsStyle="slbprimary" title={ title } key={ key } id={ 'dropdown-basic' }>
							<MenuItem
								key='none'
								eventKey='none'
								onSelect={ this.displayPerson }>
								(None)
							</MenuItem>
							<MenuItem divider/>
							{ groupPersons() }
						</DropdownHoverButton>
					</Col>
				</div>
			);
		}

		const groupDependants = () => {

			if (!this.state.groupDependants || this.state.groupDependants.length <= 0) return (<div/>);

			let listOfIds = [];

			return this.state.groupDependants.map(dependant => {
				listOfIds.push(dependant.id);
				return (
					<MenuItem
						key={dependant.id}
						eventKey={dependant.id}
						onSelect={ this.displayDependant }
						className={ dependant.active ? 'active-person' : 'inactive-person' }
						title={ dependant.active ? 'This dependant is active' : 'This dependant is NOT active'}>
						{ dependant.firstName + ' ' + dependant.lastName }
					</MenuItem>
				);
			});
		}

		const groupDependantsDropdown = () => {

			let title = 'Dependant...';
			let key = 'key';
			if (!this.state.groupDependants || this.state.groupDependants.length <= 0) return (<div/>);
			return (
				<div>
					<Col lg={2} md={3} sm={3} xs={4}>
						<DropdownHoverButton bsStyle="slbprimary" title={ title } key={ key } id={ 'dropdown-basic' }>
							<MenuItem
								key='none'
								eventKey='none'
								onSelect={ this.displayDependant }>
								(None)
							</MenuItem>
							<MenuItem divider/>
							{ groupDependants() }
						</DropdownHoverButton>
					</Col>
				</div>
			);
		}

		const tabTitle = (whichTab) => {

			let titleText = '';
			let labelText = '';
			let labelStyle = 'default';

			switch (whichTab) {
				case 'group':
					titleText = this.state.group && this.state.group.name
						? (this.state.group.name )
						: 'GROUP';

					labelText = this.state.group.isPaused ? 'PAUSED' : '';
					labelStyle = this.state.group.isPaused ? 'danger' : 'default';

					return (
						<p>
							{ titleText } <Label bsStyle={ labelStyle }>{ labelText }</Label>
						</p>
					)

				case'person':
					titleText = this.state.groupPerson.firstName || this.state.groupPerson.lastName
						? this.state.groupPerson.firstName + ' ' + this.state.groupPerson.lastName
						: 'CARER'

					if (titleText !== 'CARER') {
						labelText = this.state.groupPerson.isCarer ? '' : 'NOTCarer';
						labelStyle = this.state.groupPerson.isCarer ? 'default' : 'warning';
					}

					return (
						<p>
							{ titleText } <Label bsStyle={ labelStyle }>{ labelText }</Label>
						</p>
					)

				case'dependant':
					titleText = this.state.groupDependant && this.state.groupDependant.lastName
						? this.state.groupDependant.firstName + ' ' + this.state.groupDependant.lastName
						: 'DEPENDANT'

					return (
						<p>
							{ titleText } <Label bsStyle={ labelStyle }>{ labelText }</Label>
						</p>
					)
			}

			return (<div/>);

		}

		return (
			<div>
				<PanelContainer>
					<Panel>
						<PanelBody style={{padding: 0}}>
							<Grid>
								<Row>
									<Col xs={12} sm={12} md={12}>
										<Row>
											<Col xs={4} sm={3} md={3} lg={2}>
												{ groupsDropdown }
											</Col>
											<Col xs={4} sm={3} md={3} lg={2}>
												{ groupPersonsDropdown() }
											</Col>
											<Col xs={4} sm={3} md={3} lg={2}>
												{ groupDependantsDropdown() }
											</Col>
										</Row>
										<p></p>
										<Row>
											<Tabs  id="controlled-tab-example"
											       activeKey={this.state.activeTab}
											       onSelect={this.handleSelectGroupsTab}>
												<Tab eventKey={ 'group' }
												     title = { tabTitle('group') }
											         >
													<p></p>
													{ groupForm }
												</Tab>
												<Tab eventKey={ 'person' }  disabled={!this.state.group.id || this.state.group.id == 'none'}
												                            title = { tabTitle('person') }>
													<p></p>
													{ groupPersonForm() }
												</Tab>
												<Tab eventKey={ 'dependant' } disabled={!this.state.group.id || this.state.group.id == 'none'}
												                               title = { tabTitle('dependant') }>
													<p></p>
													{ groupDependantForm() }
												</Tab>
												<Tab eventKey={ 'finance' } disabled={!this.state.group.id || this.state.group.id == 'none'}
												                            title='Payments'>
													<p></p>
													{ financialsForm() }
												</Tab>
											</Tabs>
											<p></p>
										</Row>
									</Col>
								</Row>
							</Grid>
						</PanelBody>
					</Panel>
				</PanelContainer>

			</div>
		);
	}
}

const mapStateToProps = state => {
	return state;
}
const mapDispatchToProps = dispatch => {
	return {
		notify: (message, options) => dispatch(Action.notify(message, options)),
	}
}

Groups = connect (
	mapStateToProps,
	mapDispatchToProps,
)(Groups)

export default Groups


