// Third party imports
import React from 'react';
import axios from 'axios';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import {
	Row,
	Col,
	Icon,
	Grid,
	Form,
	Button,
	FormGroup,
	FormControl,
	ControlLabel
} from '@sketchpixy/rubix';
let _ = require('lodash');

// Own imports
import * as Action from '../common/actions';
import * as logger from '../common/logger';


class Login extends React.Component {

	constructor(props) {

		super(props);

		this.state = {
			email: '',
			password: '',
		};

		this.handleChange = this.handleChange.bind(this);
		this.handleLogin = this.handleLogin.bind(this);
	}

	componentDidMount(props) {}

	componentDidUpdate(props) {}

	//
	// Handlers
	//
	handleChange(event) {
		let stateUpdate = {};
		stateUpdate[event.target.id] = event.target.value;

		this.setState(stateUpdate);
	}

	handleLogin(event) {
		event.preventDefault();

		let self = this;

		let environment = this.props.environment;
		if (_.isEmpty(environment)) return;

		if (self.state.email.length > 0 && self.state.password.length > 0) {
			logger.debug('login attempt with:', environment, self.state);

			axios
				.post(environment.baseUri + '/login', self.state)
				.then(response => {
					logger.info('login success response', response);
					if (response.status == 200) {

						browserHistory.push({
							pathname: '/i/calendar',
							state: {
								school: response.data.school,
								venue: response.data.venue,
								person: response.data.person,
								token: response.data.token
							}
						} );

					} else {
						this.props.notify('Unknown error during login');
					}
				})
				.catch(error => {
					console.log('login error response', error.message, error.response);
					logger.info('login error response', error.message, error.response);
					let reason = error.message || error.response && error.response.data || 'Server error';

					// Check on 404 errors
					if (reason.indexOf('404') > -1) reason = 'The network or server was not found. Is internet ok? Possibly the server is down?';
					this.props.notify('Login has failed: ' + reason, { level: 'warning' });
				});
		}
	}

	//
	// Renderers
	//
	render() {

		return (
			<div>

				{/*<Provider store={ store }>*/}

					<Form inline>
						<FormGroup controlId="email">
							<Col sm={10}>
								<FormControl type="email" placeholder="Email or username" value={this.state.email} onChange={this.handleChange}/>
							</Col>
						</FormGroup>

						<FormGroup controlId="password">
							<Col sm={10}>
								<FormControl type="password" placeholder="Password" value={this.state.password} onChange={this.handleChange} />
							</Col>
						</FormGroup>

						<FormGroup>
							<Col smOffset={2} sm={10}>
								<Button bsStyle="slbprimary" type="submit" onClick={this.handleLogin}>
									Login
								</Button>
							</Col>
						</FormGroup>
					</Form>

				{/*</Provider>*/}
			</div>
		);
	}
}

const mapStateToProps = state => {
	return state;
}
const mapDispatchToProps = dispatch => {
	return {
		notify: (message, options) => dispatch(Action.notify(message, options)),
	}
}

Login = connect (
	mapStateToProps,
	mapDispatchToProps,
)(Login)

export default Login



