// Third party imports
import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import DatePicker from 'react-datepicker';
import {
	Row,
	Col,
	Nav,
	Tab,
	Icon,
	Grid,
	Clearfix,
	Panel,
	NavItem,
	MenuItem,
	PanelLeft,
	PanelBody,
	LoremIpsum,
	PanelRight,
	PanelHeader,
	PanelFooter,
	NavDropdown,
	PanelContainer,
	PanelTabContainer,
	DropdownHoverButton,
	DropdownButton,
	Form,
	FormGroup,
	FormControl,
	ControlLabel,
	Checkbox,
	Button,
	InputGroup,
	ButtonGroup,
	ListGroup,
	ListGroupItem,
	Modal,
	ButtonToolbar,
	OverlayTrigger,
	Tooltip,
	Popover
} from '@sketchpixy/rubix';
let _ = require('lodash');
let moment = require('moment');
let tinycolor = require('tinycolor2');

// Own imports
import * as Action from '../common/actions';
import debounceAxios from '../common/debounceAxios';
import LevelsDropdown from '../common/levelsDropdown';
import * as logger from '../common/logger';
import StaffDropdown from '../common/staffDropdown';

/*
 * TODO need to restructure:
 * TODO     an update returns a class -> update state
 * TODO     see warnings during render
 */

const localStyles = {

	repeatTitle: {
		display: 'inline-block',
		width: '20%',
		textAlign: 'right',
		paddingRight: '20px',
	},
	repeatDays: {
		display: 'inline-block',
		width: '80%',
	},
	generateText: {
		display: 'inline-block',
		width: '80%',
		color: 'lightgrey',
		fontStyle: 'italic',
	},
	exampleText: {
		display: 'inline',
		width: '80%',
		color: 'lightgrey',
		fontStyle: 'italic',
		paddingLeft: '10px',
	}
}

class Calendar extends React.Component {

	constructor(props) {

		super(props);
		logger.info('CALENDARCONST', this.props);

		this.state = {
			action: '',
			calendar: [],
			class: {},
			classes: [],
			date: null,
			level: {},
			showChangeSwimClassModal: false,
			term: '',
			terms: [],
		};

		// Build the state to represent calendar
		this.slot = {};
		this.slot.size = {x:40, y:60};              // pixels: must match the draggable grid and css slot sizes
		this.slot.duration = 15;                    // minutes
		let hourDivider = 60 / this.slot.duration;
		let numberSlots = 24 * hourDivider;
		let numberLanes = 5;

		this.classMargin = 2;                       // pixels: must match $class-margin in css

		let calendarStructure = [];
		let slotClass = '';
		for (let y =  0; y < numberLanes; y++) {
			calendarStructure[y] = [];
			for (let x = 0; x < numberSlots + 1; x++) {
				if (y == 0 && x == 0) {
					slotClass = 'top-left';
				} else if (y == 0 && x == numberSlots + 1) {
					slotClass = 'top-right';
				} else if (y == numberLanes && x == 0) {
					slotClass = 'bottom-left';
				} else if (y == numberLanes && x == numberSlots + 1) {
					slotClass = 'bottom-right';
				} else if (y == 0 && x % hourDivider == 0) {
					slotClass = 'top-hour';
				} else if (y == numberLanes && x % hourDivider == 0) {
					slotClass = 'bottom-hour';
				} else if (y == 0) {
					slotClass = 'top';
				} else if (y == numberLanes) {
					slotClass = 'bottom';
				} else if (x == 0) {
					slotClass = 'left';
				} else if (x == numberSlots + 1) {
					slotClass = 'right';
				} else if (x % hourDivider == 0) {
					slotClass = 'hour';
				} else {
					slotClass = 'center';
				}
				
				calendarStructure[y][x] = {
					class: slotClass
				};
			}
		}

		this.state.calendarStructure = calendarStructure;

		this.handleChangeDate = this.handleChangeDate.bind(this);
		this.handleChangeClassAttr = this.handleChangeClassAttr.bind(this);
		this.handleRemove = this.handleRemove.bind(this);
		this.handleRemoveInstructor = this.handleRemoveInstructor.bind(this);
		this.handlePrint = this.handlePrint.bind(this);
		this.handleRepeats = this.handleRepeats.bind(this);
		this.repeatsOn = this.repeatsOn.bind(this);
		this.handleGenerate = this.handleGenerate.bind(this);
	}

	componentDidMount(props) {
		logger.info('CALENDARDIDMOUNT', props);

		// Set the date to a stored value
		let date = moment(localStorage.getItem('slb-calendar-date'), moment.ISO_8601);
		date = date.isValid() ? date : moment();
		this.setState({date: date});

		// Scroll the calendar to about now
		let hourNow = moment().hour();
		this.refs.swimClassesContainer.scrollLeft = this.slot.size.x * hourNow * 60 / this.slot.duration;
	}

	componentDidUpdate(prevProps, prevState) {
		logger.info('CALENDARDIDUPDATE PROPS', prevProps, this.props);
		logger.info('CALENDARDIDUPDATE STATE', prevState, this.state);

		// Get the calendar classes. The token is added from Redux store through props, possibly a short while after mounting
		if (prevProps.token !== this.props.token) this.getCalendar();

		// After render, update the click events
		$('.swim-class').draggable({
			// containment: '.swim-classes-container',
			grid: [this.slot.size.x, this.slot.size.y],
			stop: (event, ui) => {

				let shiftedRight = Math.round(ui.position.left / this.slot.size.x);
				let shiftedDown  = Math.round(ui.position.top / this.slot.size.y);

				let coords = this.decodeCoords($(ui.helper).parent().data('coord'));
				coords.timeSlotIndex += shiftedRight;
				coords.laneIndex += shiftedDown;

				let draggedClass = {
					classId: $(ui.helper).data('class-id'),
					coords: coords
				}

				this.handleAddSwimClass(null, null, draggedClass);
			}
		});
	}

	/*
	 * Local functions
	 */
	handleChangeSwimClass = (props, event) => {
		logger.info('CALENDARHANDLECHANGESWIMCLASS', props, event);

		if (!$(props.target).data('class-id')) return;

		let oneClass = this.state.classes.filter(oneClass => oneClass.id === $(props.target).data('class-id'));

		this.setState({
			action: 'Update',
			class: oneClass[0],
			showAddSwimClassModal: true
		});

		// Find if class is not deletable
		this.axiosInstance.put('/calendar/notDeletable', {
			class: oneClass[0],
		})
			.then(response => {

				let oneClass = Object.assign({}, this.state.class, {isNotDeletable: response.data});

				oneClass.possibleRepeatStarts = this.findEarliestRepeatStart(oneClass);
				if (oneClass.repeatStarts !== 'sot') oneClass.repeatStarts = oneClass.possibleRepeatStarts;

				this.setState({
					class: oneClass,
				});
			})
			.catch(response => {
				logger.error('deletable class error response', response);
				this.props.notify(response.data, {level: 'error'});
			});
	}

	handleAddSwimClass = (props, event, draggedClass) => {
		//logger.info('CALENDARHANDLEADDSWIMCLASS', props, draggedClass);

		let coords = {};
		let action = '';
		let modal = false;

		if (draggedClass) {
			// Get the datetime and lane indexes/coords the of class being dragged
			coords = draggedClass.coords;

			// Find the class being dragged
			let oneClass = this.state.classes.filter(oneClass => oneClass.id === draggedClass.classId);
			this.setState({
				class: oneClass[0]
			});

			action = 'Drag';
			modal = false;

		} else {
			if (!$(props.target).data('coord')) return;

			// Find the datetime and lane indexes/coords the of class being updated
			coords = this.decodeCoords($(props.target).data('coord'));

			action = 'Add';
			modal = true;
		}

		// Create a updated datetime object
		let minutesSinceMidnight = (coords.timeSlotIndex - 1) * this.slot.duration;
		let hours = Math.floor(minutesSinceMidnight / 60);
		let minutes = minutesSinceMidnight % 60;
		let newDatetime = this.state.date.clone();
		newDatetime.hours(hours).minutes(minutes).seconds(0);

		// Update the class with the datetime
		this.setState({
			action: action,
			class: Object.assign({}, this.state.class, {
				datetime: newDatetime,
				laneId: coords.laneIndex,
			})
		});

		this.updateClass();

		this.setState({showAddSwimClassModal: modal})
	}

	decodeCoords = (coords) => {
		let coord = coords.split('-');
		let timeSlotIndex = parseInt(coord[0]);
		let laneIndex = parseInt(coord[1]);
		return {timeSlotIndex, laneIndex};
	}

	closeSwimClassModals = () => {
		this.setState({
			showChangeSwimClassModal: false,
			showAddSwimClassModal: false,
			class: {},
		});
	}

	handleChangeDate = (props) => {
		logger.info('CALENDARHANDLECHANGEDATE', props);
		// Can be called from the datepicker or one of the next/prev buttons

		let date = null;
		if (moment.isMoment(props)) {
			// Called from datepicker
			date = props;
		} else {
			// Called from prev/next button
			let days = parseInt($(props.target).data('move-days'));
			if (days) {
				date = this.state.date.add(days, 'days');
			}
		}

		this.setState({date: date}, () => {
			let todaysClasses = this.todaysClasses();
			this.props.notify('Found ' + todaysClasses.length + ' classes for ' + moment(date).format('Do'), {level: 'info'});
		});

		// Store the date to set the date after reload
		localStorage.setItem('slb-calendar-date', moment.utc(this.state.date).format());

		// Find the term for this date
		let nrTerms = this.setTermForDate();
		if (nrTerms > 1) {
			this.props.notify('This date falls in ' + nrTerms + ' terms. Classes will fall into the first term only', {level: 'warning'});
		}
	}

	setTermForDate = () => {
		// Find the term for the selected date
		let terms = this.state.terms.filter(term => {
			if (this.state.date.isBetween(term.startDate, term.endDate, 'day', '[]')) {
				return term;
			}
		});
		// logger.info('TERMS', terms);
		this.setState({term: terms[0]});

		// Note: don't notify here: the call from getCalendar causes an infinite loop
		return terms.length;
	}

	handleChangeClassAttr = (event) => {
		logger.info('CALENDARHandleChangeClassAttr', event.target.id, event.target.value);

		let value = event.target.value;
		let valueInt = parseInt(event.target.value);

		if (event.target.id === 'cap' && (_.isNaN(valueInt) || valueInt <= 0)) value = null;
		if (event.target.id === 'duration' && (!_.isNumber(valueInt) || valueInt <= 0)) value = null;

		let oneClass = this.state.class;
		oneClass[event.target.id] = value;
		this.setState({class: oneClass});

		// Update the database
		this.updateClass();
	}

	handleRemove(event) {
		logger.info('REMOVING', event);

		event.preventDefault();

		this.axiosInstance.put('/calendar/delete', {
			calendar: this.state.calendar,
			class: this.state.class
		})
			.then(response => {
				logger.info('delete calendar class success response', response);

				this.setState({
					class: {},
					showAddSwimClassModal: false
				});

				this.getCalendar();

				this.props.notify('Class removed');
			})
			.catch(response => {
				logger.error('delete class error response', response.response);
				this.props.notify(response, {level: 'error'});
			});
	}

	handleRemoveInstructor(instructorId) {

		let instructors = this.state.class.instructors.filter(instructor => instructor.id !== instructorId);
		let oneClass = Object.assign({}, this.state.class, {instructors: instructors});

		this.setState({
			class: oneClass,
		}, () => {
			this.updateClass(500);
		});
	}

	getCalendar = () => {
		logger.info('CALENDARGETTINGCALENDAR', this.props, this.state);

		let token = this.props && this.props.token;
		if (!token) return;

		let environment = this.props && this.props.environment;
		if (_.isEmpty(environment)) return;

		// Get the list of classes
		this.axiosInstance = axios.create({
			baseURL: environment.baseUri,
			headers: {token: token}
		});
		this.axiosInstance.post('/calendar')
			.then(response => {
				logger.info('get calendars success response', response.data);

				if (!response.data) return;

				this.setState({
					calendar: response.data.calendar,
					classes: response.data.classes,
					terms: response.data.terms
				});

				// Was a class selected? If so, update the class info in state too
				if (this.state.class && this.state.class.id) {

					let oneClass = response.data.classes.filter(oneClass => oneClass.id === this.state.class.id)[0];

					// Decide the possible start date: earliest day this week
					oneClass.possibleRepeatStarts = this.findEarliestRepeatStart(oneClass);
					if (oneClass.repeatStarts !== 'sot') oneClass.repeatStarts = oneClass.possibleRepeatStarts;

					this.setState({
						class: oneClass,
					});
				}

				// Now we have terms, set accordingly
				this.setTermForDate();
			})
			.catch((response) => {
				logger.error('calendars error response', response.response, response);
				this.props.notify(response, {level: 'error'});
			});
	}

	findEarliestRepeatStart(oneClass) {

		let earliest = moment(oneClass.datetime).day();
		if (!earliest) earliest = 7;
		let originalDow = earliest;
		oneClass.repeatDays && Array.isArray(oneClass.repeatDays) && oneClass.repeatDays.forEach(dowHuman => {
			let dow = ['__FAKE__', 'mo', 'tu', 'we', 'th', 'fr', 'sa', 'su'].indexOf(dowHuman);
			if (dow < earliest) earliest = dow;
		});

		return moment(oneClass.datetime).add(earliest - originalDow, 'days').format();
	}

	selectStaff = (staffId, event) => {
		logger.info('SELECTEDSTAFF', staffId, event.target.text, this.state);

		let selectedClass = Object.assign({}, this.state.class);

		selectedClass.instructors = selectedClass.instructors || [];
		if (selectedClass.instructors.filter(instructor => instructor.id === staffId).length === 0) {
			selectedClass.instructors.push({id: staffId});
		}

		this.setState({
			class: selectedClass
		}, () => {
			this.updateClass(500);
		});
	}

	selectLevel = (levelId, event) => {
		logger.info('SELECTEDLEVEL', levelId, event.target.text);

		// Get more level info
		this.axiosInstance.post('/level', {
			id: levelId
		})
			.then(response => {
				logger.info('find level success response', response);

				let selectedClass = this.state.class;
				selectedClass.levelId = levelId;
				selectedClass.level = response.data.level;

				this.setState({
					class: selectedClass
				});

				this.updateClass();
			})
			.catch(response => {
				logger.error('find level error response', response.response, response);
				this.props.notify(response.message || response.response.data, {level: 'error'});
			});
	}

	updateClass = (timeout = null) => {
		logger.info('UPDATECLASS', this.state.class);

		if (!this.props.token) return;

		// Update this class ie the class details at this time. No class.id => new class
		debounceAxios(
			this.axiosInstance,
			'put',
			'/calendar/update',
			{
				calendar: this.state.calendar,
				class: this.state.class
			},
			// Success callback
			response => {
				logger.info('calendar update success response', response);

				if (response.data.class) {
					this.setState({class: response.data.class});
				}
				this.getCalendar();
			},
			// Error callback
			response => {
				logger.info('calendar update error response', response.response, response);
				this.props.notify(response.response || response, {level: 'error'});
			},
			timeout,
		);
	}

	todaysClasses = () => {
		// Find the classes for today
		let todaysClasses = this.state.classes.filter(oneClass => {
			return moment(this.state.date).isSame(oneClass.datetime, 'day');
		});
		return todaysClasses;
	}

	handlePrint() {

		let oneClass = this.state.class;
		let momentDatetimeFormat = 'dddd, Do MMM h:mma';
		let momentTimeFormat = 'h:mma';

		let pdfDocument = new jspdf({
			orientation: 'portrait',
			unit: 'mm',
			format: 'a4',
		});
		pdfDocument.autoPrint();

		let x = 10, y = 20;

		pdfDocument.setFontSize(16);
		pdfDocument.text('Class list ', x, y);
		pdfDocument.lines([[180, 0]], x, y += 2);

		let swimmers = oneClass.swimmers.map(swimmer =>
			swimmer.firstName + ' ' + swimmer.lastName +
				(swimmer.hasMedicalIndication ? ' (has a medical indication)' : '' )
		);
		if (swimmers.length === 0) swimmers.push('No swimmers enrolled yet');

		let instructors = oneClass.instructors && oneClass.instructors.length
			? oneClass.instructors.map(inst => inst.displayName).join(', ')
			: 'No instructor yet';

		pdfDocument.setFontSize(11);
		pdfDocument.text([
			oneClass.level.name || 'No level selected yet',
			moment(oneClass.datetime).format(momentDatetimeFormat) + ' - ' +
			moment(oneClass.datetime).add(oneClass.level.duration || 0, 'minutes').format(momentTimeFormat),
			(oneClass.level.duration || '0') + ' minutes',
			oneClass.swimmers.length + ' / ' + (oneClass.cap || 'no-cap') + ' swimmers',
			'Lane ' + (oneClass.laneId || '?'),
			instructors,
			oneClass.note || '',
			'',
		], x, y += 5);

		pdfDocument.text('Swimmers:', x, y += 40)
		pdfDocument.lines([[18, 0]], x, y += 1);
		pdfDocument.text([
			...swimmers
		], x, y += 5);

		// logger.info('FONTS', pdfDocument.getFontList());
		// logger.info('JSPDF', this.state, pdfDocument);

		pdfDocument.save('classList-' + oneClass.level.name + '-' + moment(oneClass.datetime).format(momentDatetimeFormat) + '.pdf');
	}

	handleRepeats(repeatType, data) {
		// logger.info('HANDLEREPEEATS', repeatType, data && data.target && data.target.value);

		let oneClass = Object.assign({}, this.state.class);

		switch (repeatType) {
			case 'single':
				oneClass.isRepeating = false;
				this.setState({
					repeatType: repeatType,
				});
				break;

			case 'repeated':
			    oneClass.isRepeating = true;
			    this.setState({
					repeatType: repeatType,
				});

				// Default settings for a repeated class
				oneClass.repeatStarts = oneClass.repeatStarts || 'sot';
				oneClass.repeatEnds = oneClass.repeatEnds || 'eot';
				oneClass.bookSeries = oneClass.bookSeries || 'weekly';
				oneClass.repeatDays = oneClass.repeatDays || [];
				if (oneClass.repeatDays === 0) oneClass.repeatDays.push(moment(oneClass.datetime).format('dd'));
				break;

			case 'mo':
			case 'tu':
			case 'we':
			case 'th':
			case 'fr':
			case 'sa':
			case 'su':
				if (!oneClass.repeatDays) oneClass.repeatDays = [];
				let dayIndex = oneClass.repeatDays.indexOf(repeatType);
				if (dayIndex < 0) {
					oneClass.repeatDays.push(repeatType);           // Add a day
				} else {
					oneClass.repeatDays.splice(dayIndex, 1);        // Remove a day
				}

				// Decide the possible start date: earliest day this week
				oneClass.possibleRepeatStarts = this.findEarliestRepeatStart(oneClass);
				break;

			case 'sot':
				oneClass.repeatStarts = 'sot';
				break;

			case 'stoday':
				oneClass.repeatStarts = oneClass.possibleRepeatStarts || oneClass.datetime;
				break;

			case 'eot':
				oneClass.repeatEnds = 'eot';
				break;

			case 'enumber':
				let nr;
				nr = data && data.target && data.target.value;
				nr = nr && parseInt(nr);
				if (!nr || isNaN(nr) || nr < 0) nr = 1;
				oneClass.repeatEnds = nr;
				break;

			case 'bdays':
				oneClass.bookSeries = 'days';
				break;

			case 'bweekly':
				oneClass.bookSeries = 'weekly';
				break;
		}

		this.setState({
			class: oneClass,
		}, () => {
			this.updateClass(500);
		});
	}

	repeatsOn(day) {
		// Is this day marked as a repeat day
		if (this.state.class.repeatDays && this.state.class.repeatDays.indexOf(day) > -1) return true;

		// Is this day the day of the class
		if (moment(this.state.class.datetime).format('dd').toLowerCase() === day) {
			if (this.state.class.repeatDays && this.state.class.repeatDays.indexOf(day) < 0) this.state.class.repeatDays.push(day)
			return true;
		}

		return false;
	}

	/*
	 * Generate the repeating classes
	 */
	handleGenerate() {

		logger.info('HANDLEGENERATE', this.state.class);

		this.axiosInstance.put('/calendar/generate', {
			calendar: this.state.calendar,
			class: this.state.class,
		})
			.then(response => {
				logger.info('generating classes response', response);

				this.props.notify(response.data);
			})
			.catch(response => {
				logger.error('Generating classes error response', response);
				this.props.notify(response, {level: 'error'});
			});
	}

	/*
	 * Renderers
	 */
	render() {

		let momentDatetimeFormat = 'dddd, Do MMM h:mma';

		logger.info('RENDERING CALENDAR', this.state);

		const basins = () => {
			let basins = this.state.calendarStructure.map((lane, index) => {
				return(
					<div key={ 'basin' + index.toString() } className='basin'>
						<input type='text' value={ 'Lane ' + (index +1).toString() }  onChange={ () => false }/>
					</div>
				);
			});
			return (
				<div>
					<div className="basin-header"></div>
					{ basins }
				</div>
			)
		}

		const calendarTimesHeader = () => {
			let timesHeader = this.state.calendarStructure[0].map((slot, index) => {
				if (index == 0) {
					return;
				}
				// On the hour, provide a time
				if ((index - 1) * this.slot.duration % 60 === 0) {
					let timeString = (index - 1) * this.slot.duration / 60;
					if (timeString === 0)  timeString = '12am';
					if (timeString < 12)   timeString = timeString + 'am';
					if (timeString === 12) timeString = timeString + 'pm';
					if (timeString >= 12)  timeString = timeString - 12 + 'pm';
					return (
						<div className='times-header' key={ 'timesheader' + index.toString() }>{ timeString }</div>
					);
				}
				return (
					<div className='times-header' key={ 'timesheader' + index.toString() }></div>
				);
			});
			return (
				<div>{ timesHeader }</div>
			)
		}

		// Create the calendar for one day.
		// Create the DOM calendar structure, the lanes and timeslots. At each step find the relevant classes and add to the DOM
		const calendar = () => {

			// Find the classes for today
			let todaysClasses = this.todaysClasses();

			// Loop through the structure and add classes where they should be
			let calendarItems = this.state.calendarStructure.map((lane, laneIndex) => {

				// Find the classes for this lane
				let lanesClasses = todaysClasses.filter(oneClass => {
					return oneClass.laneId === laneIndex.toString();
				});

				// Loop through the lane slots and add any classes
				let oneLane = lane.map((slot, index) => {
					// Skip the first column that contains lane name
					if (index === 0) return;

					// Find the classes that start now
					let minutesSinceMidnight = (index - 1) * this.slot.duration;
					let classesNow = lanesClasses.filter(oneClass => {
						let starttime = moment(oneClass.datetime);
						let minutesSince = starttime.hours() * 60 + starttime.minutes();
						return (minutesSince >= minutesSinceMidnight && minutesSince < (minutesSinceMidnight + this.slot.duration));
					});

					let thisClass = (<div/>);
					if (classesNow.length > 0) {
						// For now, only show the first class
						// TODO handle overlapping classes

						// Calculate the width of this class
						let classWidth = '';
						let duration = parseInt(classesNow[0].duration || classesNow[0].level.duration);
						if (duration) {
							classWidth = Math.round(duration / this.slot.duration * this.slot.size.x - 2 * this.classMargin) + 'px';
						}

						// Get the color for this class
						let classColor = classesNow[0].level.color;
						let textColor = tinycolor(classColor).isLight() ? 'black' : 'lightgrey';

						// Generate the class DOM (JSX)
						let instructors = classesNow[0].instructors && classesNow[0].instructors.length
							? classesNow[0].instructors.map(inst => inst.displayName).join(', ')
							: '';

						thisClass = (
							<OverlayTrigger trigger={["hover", "focus"]}
							                placement="right"
							                overlay={<Popover bsStyle='width:100px;'
							                                  id='popover-bottom-2'
							                                  title={ classesNow[0].level ? classesNow[0].level.name : '' }>
								                        { instructors }<br/>
								                        { classesNow[0].swimmers.length } enrolled<br/>
								                        { classesNow[0].cap || classesNow[0].level.cap || 'No' } cap<br/>
								                        { classesNow[0].duration || classesNow[0].level.duration || '?' } minutes<br/>
								                        { classesNow[0].note }<br/>
							                            { classesNow[0].swimmers.length == 0 ? 'No swimmers enrolled yet' :
								                            'Enrolled: ' +
								                            classesNow[0].swimmers.map(swimmer => swimmer.firstName).join(', ') }
							                         </Popover>}>
								<div className={ classesNow.length > 1 ? 'swim-class slbdanger' : 'swim-class' }
								     onClick={ this.handleChangeSwimClass }
								     data-class-id={ classesNow[0].id }
								     key={ classesNow[0].id }
								     style={{ 'width': classWidth, 'backgroundColor':classColor, 'color':textColor }}
									>
									<span className="swim-class-detail" data-class-id={classesNow[0].id}>
										{classesNow[0].level ? classesNow[0].level.name : ''}
							        </span>
									<br/>
									<span className="swim-class-detail" data-class-id={classesNow[0].id}>
										{ instructors }
									</span>
									<br/>
									<span className="swim-class-detail" data-class-id={classesNow[0].id}>
										{ classesNow[0].swimmers.length }/{classesNow[0].cap || classesNow[0].level.cap || '?'}
									</span>
								</div>
							</OverlayTrigger>
						);
					}
					return(
						<div className={slot.class}
						     onClick={this.handleAddSwimClass}
						     data-coord={index.toString() + '-' + laneIndex.toString()}
						     key={ 'onelane' + index.toString() }>
							{ thisClass }
						</div>
					);
				});
				return (
					<div className='swim-lane' key={ 'swimlane' + laneIndex.toString() }>
						{ oneLane }
					</div>
				);
			});

			return calendarItems;
		}

		const classDetailsLevelStaff = () => {
			return (
				<div>
					{/* The levels dropdown */}
					<Row>
						<Col sm={4}>
							<LevelsDropdown onSelect={ this.selectLevel }/>
						</Col>
					</Row>

					{/* Class details */}
					<Row>
						<Col sm={12}>
							<InputGroup>
								<FormControl type="text"
								             id="level"
								             placeholder="Level"
								             disabled
								             value={ this.state.class.level ? this.state.class.level.name : ''  }/>
							</InputGroup>
						</Col>
						<Col sm={12}>
							<InputGroup>
								<FormControl type="text"
								             id="cap"
								             placeholder={ "Cap " + (this.state.class.level && this.state.class.level.cap
									             ? "(default " + this.state.class.level.cap + ")"
									             : '') }
								             onChange={ this.handleChangeClassAttr }
								             value={ this.state.class.cap }/>
							</InputGroup>
						</Col>
						<Col sm={12}>
							<InputGroup>
								<FormControl type="text"
								             id="duration"
								             placeholder={ "Duration " + (this.state.class.level && this.state.class.level.duration
									             ? "(default " + this.state.class.level.duration + ")"
									             : '') }
								             onChange={ this.handleChangeClassAttr }
								             value={ this.state.class.duration }/>
							</InputGroup>
						</Col>
						<Col sm={12}>
							<InputGroup>
								<FormControl type="text"
								             id="note"
								             placeholder="Notes"
								             onChange={this.handleChangeClassAttr}
								             value={this.state.class.note ? this.state.class.note : ''}/>
							</InputGroup>
						</Col>
					</Row>

					<Row>
						.
					</Row>

					{/* The staff dropdown */}
					<Row>
						<Col sm={12}>
							<StaffDropdown role={ 'instructor' } onSelect={ this.selectStaff }/>
						</Col>
					</Row>

					{/* The list of instructors */}
					<Row>
						{ this.state.class.instructors && this.state.class.instructors.map(instructor => {
							return (
								<Col sm={12} key={ instructor.id }>
									<InputGroup>
										<FormControl type="text"
										             id="instructor"
										             placeholder="Instructor"
										             disabled
										             value={ instructor.displayName || '' }/>

										<InputGroup.Button>
											<Button
												bsStyle="slbneutral"
												type="button"
												onClick={ () => this.handleRemoveInstructor(instructor.id) }>
												<Icon id={ instructor.id } glyph='icon-fontello-minus' />
											</Button>
										</InputGroup.Button>
									</InputGroup>
								</Col>
							)
						})}
					</Row>

				</div>
			);
		}

		const classEditModal = (
			<Modal show={this.state.showAddSwimClassModal} onHide={this.closeSwimClassModals}>
				<Modal.Header closeButton>
					<Modal.Title>
						{this.state.action} Swim Class Details
						<span style={{ fontSize: 10 }}>
			                     { ' ' + moment(this.state.class.datetime).format(momentDatetimeFormat) + ', lane ' + this.state.class.laneId }
		                     </span>
					</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Row>
						{/* Left hand column*/}
						<Col sm={4}>
							{ classDetailsLevelStaff() }
						</Col>

						{/* Middle column*/}
						<Col sm={4}>
							<Row>
								<Button disabled outlined>Enrolled</Button>
							</Row>
							<Row>
								<Col sm={12} collapseLeft>
									{ this.state.class.swimmers && this.state.class.swimmers.length
										? this.state.class.swimmers.map(swimmer => {
											return (
												<div key={ swimmer.id }>
													{ swimmer.firstName + ' ' + swimmer.lastName + ' '}
													{ swimmer.hasMedicalIndication ? <Icon glyph='icon-ikons-first-aid' /> : '' }
												</div>
											)
										})
										: ''
									}
								</Col>
							</Row>
						</Col>

						{/* Right hand column*/}
						<Col sm={4}>
							<Row>
								<Button disabled outlined>Waiting List</Button>
							</Row>
							<Row>
								<Col sm={12} collapseLeft collapseRight>
									{ this.state.class.waiters && this.state.class.waiters.length
										? this.state.class.waiters.map(waiter => {
											return (
												<div key={ waiter.id }>
													{ waiter.firstName + ' ' + waiter.lastName + ' '}
													{ waiter.hasMedicalIndication ? <Icon glyph='icon-ikons-first-aid' /> : '' }
													<span style={{ fontSize: 8 }}>{ moment(waiter.joinDate).format('Do MMM h:mma') }</span>
												</div>
											)
										})
										: ''
									}
								</Col>
							</Row>
						</Col>
					</Row>
					<Row>
						.
					</Row>
					<Row>
						{this.state.class.repeatId ?
							<div>
							<Col sm={4}>
								<Button disabled outlined>Repeated Class</Button>
							</Col>
							<Col sm={8}>
								<div style={ localStyles.generateText }>
									{ this.state.class.repeatShortDescription }
								</div>
							</Col>
							</div>
						:
							<Col sm={12}>
								<DropdownButton bsStyle="slbprimary" title={'Repeats'} key={'key'}
								                id={'repeats-dropdown'}>
									<MenuItem
										key='single'
										eventKey='single'
										onSelect={this.handleRepeats}>
										Single class
									</MenuItem>
									<MenuItem
										key='repeated'
										eventKey='repeated'
										onSelect={this.handleRepeats}>
										Repeated class
									</MenuItem>
								</DropdownButton>
								{!this.state.class.isRepeating ?
									<InputGroup>
										<FormControl type="text"
										             id="single"
										             placeholder="Repeats type"
										             disabled
										             value={'Single class'}/>
									</InputGroup>
									:
									<Form>
										<div style={localStyles.repeatTitle}>
											{'Every: '}
										</div>
										<div style={localStyles.repeatDays}>
											<Checkbox inline id='mo' checked={this.repeatsOn('mo')}
											          onChange={() => this.handleRepeats('mo')}>Mo</Checkbox>
											<Checkbox inline id='tu' checked={this.repeatsOn('tu')}
											          onChange={() => this.handleRepeats('tu')}>Tu</Checkbox>
											<Checkbox inline id='we' checked={this.repeatsOn('we')}
											          onChange={() => this.handleRepeats('we')}>We</Checkbox>
											<Checkbox inline id='th' checked={this.repeatsOn('th')}
											          onChange={() => this.handleRepeats('th')}>Th</Checkbox>
											<Checkbox inline id='fr' checked={this.repeatsOn('fr')}
											          onChange={() => this.handleRepeats('fr')}>Fr</Checkbox>
											<Checkbox inline id='sa' checked={this.repeatsOn('sa')}
											          onChange={() => this.handleRepeats('sa')}>Sa</Checkbox>
											<Checkbox inline id='su' checked={this.repeatsOn('su')}
											          onChange={() => this.handleRepeats('su')}>Su</Checkbox>
										</div>
										{this.state.class.repeatDays && this.state.class.repeatDays.length > 1 ?
											<div>
												<div style={localStyles.repeatTitle}>
													{'Book a series: '}
												</div>
												<div style={localStyles.repeatDays}>
													<Checkbox inline id='bweekly'
													          checked={this.state.class.bookSeries === 'weekly'}
													          onChange={() => this.handleRepeats('bweekly')}>Weekly</Checkbox>
													<Checkbox inline id='bdays'
													          checked={this.state.class.bookSeries === 'days'}
													          onChange={() => this.handleRepeats('bdays')}>On all these
														days</Checkbox>
													<div style={ localStyles.exampleText }>
														{ this.state.class.exampleDescription ? 'E.g: ' + this.state.class.exampleDescription : null }
													</div>
												</div>
											</div>
											: null}
										<div style={localStyles.repeatTitle}>
											{'Starts: '}
										</div>
										<div style={localStyles.repeatDays}>
											<Checkbox inline id='sot' checked={this.state.class.repeatStarts === 'sot'}
											          onChange={() => this.handleRepeats('sot')}>Start of term</Checkbox>
											<Checkbox inline id='stoday'
											          checked={ this.state.class.repeatStarts && this.state.class.repeatStarts !== 'sot' }
											          onChange={ () => this.handleRepeats('stoday') }>
												{ moment(this.state.class.possibleRepeatStarts || this.state.class.datetime).format('dd Do MMM') }
											</Checkbox>
										</div>
										<div style={ localStyles.repeatTitle }>
											{'Until: '}
										</div>
										<div style={localStyles.repeatDays}>
											<Checkbox inline id='eot'
											          checked={this.state.class.repeatEnds === 'eot'}
											          onChange={() => this.handleRepeats('eot')}>End of term</Checkbox>
											<Checkbox inline id='enumber'
											          checked={this.state.class.repeatEnds !== 'eot'}
											          onChange={() => this.handleRepeats('enumber')}>
												Total of {this.state.class.repeatEnds !== 'eot'
												? <FormControl type="text"
												               id="enumber"
												               bsClass={ 'fakeClassForceRemovalStyling' }
												               onChange={event => this.handleRepeats('enumber', event)}
												               value={ this.state.class.repeatEnds }/>
												: '___'
											} classes
											</Checkbox>
										</div>
										<div style={ localStyles.repeatTitle }>
											<Button onClick={ this.handleGenerate }>Generate</Button>
										</div>
										<div style={ localStyles.generateText }>
											After generation, classes can only be updated individually
										</div>
									</Form>
								}
							</Col>
						}
					</Row>
				</Modal.Body>

				<Modal.Footer>
			            <span>
			                <Button bsStyle="slbneutral"
			                        type="button" onClick={ this.handleRemove }
			                        className={ this.state.class && !this.state.class.id ? "hide" : "" }
			                        disabled={ this.state.class.isNotDeletable ? true : false }>
				                <Icon glyph='icon-fontello-trash-1' />
				                { this.state.class.isNotDeletable || "Remove Class" }
			                </Button>
			            </span>
					<span>
			                <Button onClick={ this.handlePrint }>Print</Button>
			                <Button onClick={ this.closeSwimClassModals }>Close</Button>
			            </span>
				</Modal.Footer>
			</Modal>
		)

		return (
            <div>
	            {/* Date bar */}
	                <Grid collapse>
			            <Row>
				            <Col xs={12} xsPush={0} sm={12} smPush={3} md={10} mdPush={2} lg={10} lgPush={2}>

				            	<Col xs={1} sm={1} md={2} lg={2} collapseLeft collapseRight style={{ textAlign: 'center' }}>
						            <Button bsStyle="slbneutral"  type="button" data-move-days={-7} onClick={this.handleChangeDate}>
							            <Icon glyph='icon-ikons-arrow-left' data-move-days={-7} />
							            <Icon glyph='icon-ikons-arrow-left' data-move-days={-7} style={{ marginLeft: -9 }}/>
						            </Button>
					                <Button bsStyle="slbneutral"  type="button" data-move-days={-1} onClick={this.handleChangeDate}><Icon glyph='icon-ikons-arrow-left' data-move-days={-1} /></Button>
					           	</Col>

					            <Col xs={3} sm={3} md={2} lg={2} collapseLeft collapseRight style={{ textAlign: 'center' }}>
						            <DatePicker id="startDate"
						                        onChange={this.handleChangeDate}
						                        selected={this.state.date}
						                        dateFormat="DD-MM-YYYY"
						                        placeholderText="Date" />
					            </Col>

						        <Col xs={1} xsPush={1} sm={1} smPull={1} md={2} lg={2} collapseLeft collapseRight style={{ textAlign: 'center' }}>
					                <Button bsStyle="slbneutral" type="button" data-move-days={1} onClick={this.handleChangeDate}>
						                <Icon glyph='icon-ikons-arrow-right' data-move-days={1} />
					                </Button>
					                <Button bsStyle="slbneutral" type="button" data-move-days={7} onClick={this.handleChangeDate}>
						                <Icon glyph='icon-ikons-arrow-right' data-move-days={7} />
						                <Icon glyph='icon-ikons-arrow-right' data-move-days={7} style={{ marginLeft: -9 }}/>
					                </Button>
					           	</Col>

					            <Col xs={6} xsPush={1} sm={6} smPush={1} md={6} lg={6} collapseLeft>
						            {this.state.term ? 'Is in term: ' + this.state.term.name : '(Not in a term)'}
						            <p></p>
						        </Col>
					        </Col>
				        </Row>
		            </Grid>
	            
	            {/* Calendar */}
	            <Row>
		            <Col md={2} sm={3}>
			            { basins() }
		            </Col>
		            <Col md={10} sm={9}>

			            <div className="swim-classes-container" ref={ 'swimClassesContainer' }>

				            { calendarTimesHeader() }

				            { calendar() }

			            </div>
		            </Col>
	            </Row>

	            {/* Edit a Class Modal */}
	            { classEditModal }
            </div>
		);
	}
}

const mapStateToProps = state => {
	return state;
}
const mapDispatchToProps = dispatch => {
	return {
		notify: (message, options) => dispatch(Action.notify(message, options)),
	}
}

Calendar = connect (
	mapStateToProps,
	mapDispatchToProps,
)(Calendar)

export default Calendar
