// Third party imports
import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import DatePicker from 'react-datepicker';
import { validate } from 'validate.js';
import {
	Row,
	Col,
	Icon,
	Grid,
	Panel,
	PanelBody,
	PanelContainer,
	Form,
	FormGroup,
	FormControl,
	ControlLabel,
	Checkbox,
	Button,
	InputGroup,
	ButtonGroup,
	OverlayTrigger,
	Tooltip
} from '@sketchpixy/rubix';

let moment = require('moment');


// Own imports
import * as Action from '../common/actions';
// import { env } from '../environment';
import StaffDropdown from '../common/staffDropdown';
import debounceAxios from '../common/debounceAxios';
import * as logger from '../common/logger';


class Staff extends React.Component {

	constructor(props) {

		super(props);
		logger.info('STAFFCONST', props);

		this.state = {
			staffMember: props.staffMember || {},
			schedule: props.schedule || [],
		}

		this.axiosInstance = null;

		// Bind 'this' to local functions
		this.handleChange = this.handleChange.bind(this);
		this.handleRemove = this.handleRemove.bind(this);
		this.displayStaffMember = this.displayStaffMember.bind(this);
	}

	handleChange(event) {

		let prevStaffMember = this.state.staffMember;
		let newValue;

		if (moment.isMoment(event)) {
			// A moment object is passed by the datepicker (for dob)
			newValue = {
				dob: event
			}
		} else {
			newValue = {
				[event.target.id]: event.target.type === 'checkbox' ? event.target.checked : event.target.value
			};
		}
		let eventTarget = event.target;

		this.setState({staffMember: Object.assign({}, prevStaffMember, newValue)}, () => {

			// Check and save the password. Only save the passwords if initiated by the save btn
			let personToSave;
			if (eventTarget.id === 'password-save-btn' ||
				eventTarget.id === 'password' ||
				eventTarget.id === 'passwordConfirm') {

				// Validate the passwords
				let validateErrors = validate(this.state.staffMember, {
					password: { length: {minimum: 8}},
					passwordConfirm: {equality: 'password'},
				});

				if (validateErrors) {
					this.props.notify(validateErrors.password || validateErrors.passwordConfirm, {level: 'error'});
					return;
				}

				// If it's the save button, keep the passwords and save, otherwise only notify the user
				if (eventTarget.id !== 'password-save-btn') {
					this.props.notify('Don\'t forget to save the password for ' + this.state.staffMember.displayName, {level: 'warning'});
					return;
				}
				personToSave = this.state.staffMember;
				personToSave.setPassword = true;

			} else {
				// Remove the passwords but save the rest
				personToSave = Object.assign({}, this.state.staffMember);
				personToSave.password = '';
				personToSave.passwordConfirm = '';
				personToSave.setPassword = false;
			}

			logger.info('UPDATING+VALIDATING', personToSave);
			debounceAxios(
				this.axiosInstance,
				'put',
				'/staff-member/update',
				{
					staffMember: personToSave
				},
				// Success callback
				response => {
					logger.info('update staff success response', response);
					if (response.data.errors) {
						this.props.notify(response.data.errors, {
							level: (response.data.isValid ? 'warning' : 'error')
						});
					}

					if (response.data.staffMember) {
						let staffMember = response.data.staffMember;
						staffMember.password = '';
						staffMember.passwordConfirm = '';
						this.setState({
							staffMember: staffMember,
						});
					}

					logger.info('About to notify staff saved');
					this.props.notify('Staff saved');
				},
				// Error callback
				response => {
					logger.info('update staff error response', response);
					this.props.notify(response, {level: 'error'});
				}
			);
		});
	}

	handleRemove(event) {
		logger.info('STAFF REMOVING', event, );

		event.preventDefault();

		this.axiosInstance.put('/staff-member/delete', {
				staffMember: this.state.staffMember
			})
			.then(response => {
				logger.info('delete staff success response', response);

				this.setState({
					staffMember: {}
				});

				this.displayStaffMember('none');

				this.props.notify('Staff removed');
			})
			.catch(response => {
				logger.info('delete staff error response', response);
			});
	}

	displayStaffMember (staffId, event) {

		// Clear relevant state in case no info is returned
		this.setState({
			staffMember: {},
			schedule: [],
		});

		// Set up axios
		if (!this.axiosInstance) {
			let token = this.props && this.props.token;
			// let venue = props && props.venue;
			if (!token) return;

			let environment = this.props && this.props.environment;
			if (_.isEmpty(environment)) return;

			// Setup the axios instance
			this.axiosInstance = axios.create({
				baseURL: environment.baseUri,
				headers: {token: token}
			});
		}

		// Get the staff member's info
		this.axiosInstance.post('/staff-member', {
			id: staffId,
			venue: this.props.venue
		})
		.then(response => {
			logger.info('single staffMember success response', response);

			this.setState({
				staffMember: response.data.staffMember,
				schedule: response.data.schedule,
			});
		})
		.catch(response => {
			logger.info('single staffMember error response', response);
			this.props.notify(response, {level: 'error'});
		});
	}

	//
	// Renderers
	//
	render() {

		const staffForm = (
			<Form horizontal onChange={ this.handleChange }>
				<FormGroup controlId="firstName">
					<Col componentClass={ControlLabel} xs={3} sm={3} md={3} lg={3}>First Name</Col>
					<Col xs={9} sm={9}><FormControl type="text" placeholder="First Name" value={ this.state.staffMember.firstName || '' } /></Col>
				</FormGroup>

				<FormGroup controlId="lastName">
					<Col componentClass={ControlLabel} xs={3} sm={3} md={3} lg={3}>Last Name</Col>
					<Col xs={9} sm={9}><FormControl type="text" placeholder="Last Name" value={ this.state.staffMember.lastName || '' }/></Col>
				</FormGroup>

				<FormGroup controlId="displayName">
					<Col componentClass={ControlLabel} xs={3} sm={3} md={3} lg={3}>Display</Col>
					<Col xs={9} sm={9}><FormControl type="text" placeholder="Display Name" value={ this.state.staffMember.displayName || '' }/></Col>
				</FormGroup>

				<FormGroup controlId="phoneNumber">
					<Col componentClass={ControlLabel} xs={3} sm={3} md={3} lg={3}>Phone</Col>
					<Col xs={9} sm={9}>
						<InputGroup>
							<FormControl type="text" placeholder="Phone Number" value={ this.state.staffMember.phoneNumber || '' }/>
							<InputGroup.Button>
								<OverlayTrigger placement="bottom" overlay={ <Tooltip id="tooltipSms">Send a text</Tooltip> }>
									<Button bsStyle="slbneutral" type="button"><Icon glyph='icon-fontello-chat-1' /></Button>
								</OverlayTrigger>
							</InputGroup.Button>
						</InputGroup>
					</Col>
				</FormGroup>

				<FormGroup controlId="email">
					<Col componentClass={ControlLabel} xs={3} sm={3} md={3} lg={3}>Email</Col>
					<Col xs={9} sm={9}>
						<InputGroup>
							<FormControl type="email" placeholder="Email" value={ this.state.staffMember.email || '' }/>
							<InputGroup.Button>
								<OverlayTrigger placement="bottom" overlay={ <Tooltip id="tooltipEmail">Send an email</Tooltip> }>
									<Button bsStyle="slbneutral" type="button" href={"mailto:" + this.state.staffMember.email}><Icon glyph='icon-fontello-mail-1' /></Button>
								</OverlayTrigger>
							</InputGroup.Button>
						</InputGroup>
					</Col>
				</FormGroup>

				<FormGroup validationState={ this.state.staffMember.passwordOk }>
					<Col componentClass={ControlLabel} xs={3} sm={3} md={3} lg={3}>Password</Col>
					<Col xs={4} sm={4} md={4} lg={4} collapseRight>
						<FormControl id="password" type="password" value={ this.state.staffMember.password || '' } placeholder="8+ chars"/>
					</Col>
					<Col xs={5} sm={5} md={5} lg={5} collapseLeft>
						<InputGroup>
							<FormControl id="passwordConfirm" type="password" value={ this.state.staffMember.passwordConfirm || ''} placeholder="Retype"/>
							<InputGroup.Button>
								<OverlayTrigger placement="bottom" overlay={ <Tooltip id="tooltipPassword">Save the pass-word</Tooltip> }>
									<Button bsStyle="slbneutral" type="button" onClick={ this.handleChange } id="password-save-btn"><Icon id="password-save-btn" glyph='icon-fontello-ok' /></Button>
								</OverlayTrigger>
							</InputGroup.Button>
						</InputGroup>
					</Col>
				</FormGroup>

				<FormGroup controlId="address1">
					<Col componentClass={ControlLabel} xs={3} sm={3} md={3} lg={3}>Address line 1</Col>
					<Col xs={9} sm={9}><FormControl type="text" placeholder="E.g. street" value={ this.state.staffMember.address1 || '' }/></Col>
				</FormGroup>

				<FormGroup controlId="address2">
					<Col componentClass={ControlLabel} xs={3} sm={3} md={3} lg={3}>Address line 2</Col>
					<Col xs={9} sm={9}><FormControl type="text" placeholder="E.g. suburb or town" value={ this.state.staffMember.address2 || '' }/></Col>
				</FormGroup>

				<FormGroup controlId="address3">
					<Col componentClass={ControlLabel} xs={3} sm={3} md={3} lg={3}>Address line 3</Col>
					<Col xs={9} sm={9}><FormControl type="text" placeholder="E.g. state and postcode" value={ this.state.staffMember.address3 || '' }/></Col>
				</FormGroup>

				<FormGroup controlId="emergency1">
					<Col componentClass={ControlLabel} xs={3} sm={3} md={3} lg={3}>Emergency</Col>
					<Col xs={9} sm={9}><FormControl type="text" placeholder="E.g. emergency contact name" value={ this.state.staffMember.emergency1 || '' }/></Col>
				</FormGroup>

				<FormGroup controlId="emergency2">
					<Col componentClass={ControlLabel} xs={3} sm={3} md={3} lg={3}>Emergency</Col>
					<Col xs={9} sm={9}><FormControl type="text" placeholder="E.g. emergency contact phone" value={ this.state.staffMember.emergency2 || '' }/></Col>
				</FormGroup>

				<FormGroup controlId="dob">
					<Col componentClass={ControlLabel} xs={3} sm={3} md={3} lg={3}>Date Of Birth</Col>
					<Col xs={9} sm={9} md={9} lg={9}>
						<DatePicker id="dob"
						            onChange={ this.handleChange }
									selected={ this.state.staffMember.dob ? moment(this.state.staffMember.dob) : ''}
									openToDate={ this.state.staffMember.dob ? moment(this.state.staffMember.dob) : moment('1-Jan').subtract(20, 'years') }
							        dateFormat="DD-MM-YYYY"
							        showYearDropdown={ true }
							        placeholderText="Date Of Birth"/>
					</Col>
				</FormGroup>

				<FormGroup controlId="isInstructor">
					<Col xs={12} xsOffset={3} smOffset={3} sm={9} md={3} mdOffset={3} lg={5} lgOffset={3}>

							<Checkbox inline id='isInstructor' checked={ this.state.staffMember.isInstructor}>Instructor</Checkbox>

					</Col>
				</FormGroup>
				<FormGroup controlId="isAdmin">
					<Col xs={12} xsOffset={3} smOffset={3} sm={9} md={6} mdOffset={3} lg={5} lgOffset={3}>

							<Checkbox inline id='isAdmin' checked={ this.state.staffMember.isAdmin}>Admin</Checkbox>

					</Col>
				</FormGroup>
				<FormGroup controlId="isInactive" className={ this.state.staffMember.isActive ? "hide" : ""}>
					<Col xs={12} xsOffset={3} smOffset={3} sm={9} md={6} mdOffset={3} lg={9} lgOffset={3}>
						<p>Staff member { this.state.staffMember.displayName} is not active</p>
					</Col>
				</FormGroup>

				<Col gutterBottom collapseLeft xs={12} xsOffset={3} smOffset={3} sm={9} md={6} mdOffset={3} lg={5} lgOffset={3}>
					<ButtonGroup bsSize="sm">
						<Button bsStyle="slbneutral"  type="button" onClick={ this.handleRemove}><Icon glyph='icon-fontello-trash-1' /> Remove</Button>
					</ButtonGroup>
				</Col>

			</Form>
		);

		// per day list of times, classes and lanes
		const schedule = this.state.schedule.map(oneClass => {
			return (
				<div key={ oneClass.id }>
					<Row>
						<Col sm={3} collapseRight>
							{ moment(oneClass.datetime).format('ddd, Do MMM') }
						</Col>
						<Col sm={2} collapseRight>
							{ moment(oneClass.datetime).format('h:mma') }
						</Col>
						<Col sm={4} collapseRight>
							<span style={{'width': '10px', 'backgroundColor': oneClass.level.color}}>_</span>
							<span>
								{ ' ' + oneClass.level.name }
							</span>
						</Col>
						<Col sm={3} collapseRight>
							{ 'Lane ' + oneClass.laneId }
						</Col>
					</Row>
				</div>
			);
		});

		return (
			<div>
				<PanelContainer>
					<Panel>
						<PanelBody style={{padding: 0}}>
							<Grid>
								<Row>
									<Col xs={12} sm={6} md={6} lg={3}>
											<StaffDropdown onSelect={ this.displayStaffMember } />
											<p></p>
										
											{ staffForm }
											<p></p>
									</Col>
								
									<Col xs={12} sm={6} md={6} lg={3}>
											<h4>Schedule</h4>
											{ schedule }
										
									</Col>
								</Row>
							</Grid>
						</PanelBody>
					</Panel>
				</PanelContainer>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return state;
}
const mapDispatchToProps = dispatch => {
	return {
		notify: (message, options) => dispatch(Action.notify(message, options)),
	}
}

Staff = connect (
	mapStateToProps,
	mapDispatchToProps,
)(Staff)

export default Staff


