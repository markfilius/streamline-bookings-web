// Third party imports
import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { SortablePane, Pane } from 'react-sortable-pane';
import DatePicker from 'react-datepicker';
import {
	Row,
	Col,
	Nav,
	Tab,
	Icon,
	Image,
	Grid,
	Clearfix,
	Panel,
	NavItem,
	MenuItem,
	PanelLeft,
	PanelBody,
	LoremIpsum,
	PanelRight,
	PanelHeader,
	PanelFooter,
	NavDropdown,
	PanelContainer,
	PanelTabContainer,
	DropdownHoverButton,
	Form,
	FormGroup,
	FormControl,
	ControlLabel,
	Checkbox,
	Button,
	InputGroup,
	ButtonGroup,
	ListGroup,
	ListGroupItem,
	Label
} from '@sketchpixy/rubix';

let moment = require('moment');

// Own imports
import * as Action from '../common/actions';
// import { env } from '../environment';
import Dropzone from 'react-dropzone'
import debounceAxios from '../common/debounceAxios';
import * as logger from '../common/logger';


class Settings extends React.Component {


	constructor(props) {

		super(props);
		logger.info('SETTINGSCONST', props);

		this.state = {
			uploadMessage: '',
			school: props.school || {},
			venues: props.venues || [],
			selectedVenue: props.selectedVenue || {},
			selectedTerm: props.selectedTerm || {},
			// term: props.term || {},
			terms: props.terms || [],
			termStart: props.termStart || null,
			termEnd: props.termEnd || null,
			settingsSchool: props.route.detail == 'school' ? true : false,
			settingsVenues: props.route.detail == 'venues' ? true : false,
			settingsTerms: props.route.detail == 'terms' ? true : false,
			settingsBasins: props.route.detail == 'basins' ? true : false,
		}

		this.axiosInstance = null;

		this.logoMaxSize = 1024 * 1024;
		this.logoAllowedTypes = ['image/jpeg', 'image/png'];


		// Bind 'this' to local functions
		this.handleSchoolChange = this.handleSchoolChange.bind(this);
		this.displayVenue = this.displayVenue.bind(this);
		this.displayTerm = this.displayTerm.bind(this);
		this.handleVenueChange = this.handleVenueChange.bind(this);
		this.handleVenueRemove = this.handleVenueRemove.bind(this);
		// this.handleStartDateChange = this.handleStartDateChange.bind(this);
		// this.handleEndDateChange = this.handleEndDateChange.bind(this);
		this.handleTermChange = this.handleTermChange.bind(this);
		this.handleTermRemove = this.handleTermRemove.bind(this);
		this.handleDropZone = this.handleDropZone.bind(this);

		// Get the settings. Possibly there isn't a token yet, so this call will be empty. That get's fixed in DidUpdate
		this.getSettings(props);
	}

	componentDidMount(props) {
		logger.info('SETTINGSDIDMOUNT', props, this.state);

		this.setState({
			termStart: moment(),
			termEnd: moment().add(2, 'months'),
		});

		this.whichSettings(props);
	}

	componentDidUpdate(prevProps, prevState) {
		// Token is added from Redux store through props, possibly a short while after mounting
		if (prevProps.token !== this.props.token) this.getSettings(this.props);

		if (prevProps.route.detail !== this.props.route.detail) this.whichSettings(this.props);
	}

	//
	// School functions
	//
	handleSchoolChange(event) {
		let newValue = {};
		newValue[event.target.id] = event.target.value;
		let prevSchool = this.state.school;
		this.setState({school: Object.assign({}, prevSchool, newValue)}, () => {

			// Validation
			logger.info('VALIDATING+UPDATING', this.state.school);
			debounceAxios(
				this.axiosInstance,
				'put',
				'/setting/update',
				{
					school: this.state.school
				},
				// Success callback
				response => {
					logger.info('validate+update school success response', response);
					if (response.data.errors) {
						this.props.notify(response, {
							level: (response.data.isValid ? 'warning' : 'error')
						});
					}

					// Update the selected school
					if (response.data.school) {
						this.setState({school: response.data.school});
					}

					this.props.notify('Saved');
				},
				// Error callback
				response => {
					logger.info('validate+update school error response', response.response);
					this.props.notify(response.response.data, {level: 'error'});
				},
			);
		});
	}

	//
	// Venue functions
	//
	displayVenue (venueId, event) {
		logger.info('SELECTED', venueId, this.state);

		// Get the venue's info
		let selectedVenue = venueId === 'none'
			? [{}]
			: this.state.venues.filter(venue => venue.id === venueId);

		this.setState({
			uploadMessage: '',
			selectedVenue: selectedVenue[0]
		});
	}

	handleVenueChange(event) {
		let newValue = {};
		newValue[event.target.id] = event.target.value;
		let prevVenue = this.state.selectedVenue;
		this.setState({selectedVenue: Object.assign({}, prevVenue, newValue)}, () => {

			// Validation
			logger.info('VALIDATING+UPDATING', this.state.selectedVenue);
			debounceAxios(
				this.axiosInstance,
				'put',
				'setting/update',
				{
					selectedVenue: this.state.selectedVenue,
				},
				// Success callback
				response => {
					logger.info('validate+update venue success response', response);
					if (response.data.errors) {
						this.props.notify(response.data.errors, {
							level: (response.data.isValid ? 'warning' : 'error')
						});
					}

					// Update the selected venue in state
					if (response.data.venue) {
						this.setState({selectedVenue: response.data.venue});
					}

					// Update the dropdown
					this.getSettings();

					this.props.notify('Saved');
				},
				// Error callback
				response => {
					logger.info('validate+update venue error response', response.response);
					this.props.notify(response.response.data, {level: 'error'});
				}
			);
		});
	}

	handleVenueRemove(event, props) {
		logger.info('Venue remove', event, props, this.state);

		event.preventDefault();

		let selectedVenue = this.state.selectedVenue;

		if (!selectedVenue || !selectedVenue.id) return;

		// Remove this venue from the db for this school
		this.axiosInstance.put('/setting/delete', {
			selectedVenue: selectedVenue
		})
			.then(response => {
				logger.info('delete venue success response', response);

				// Remove the venue from state
				let newVenues = this.state.venues.filter(venue => {
					return venue.id != selectedVenue.id;
				});
				this.setState({venues: newVenues});

				this.props.notify('Location removed');
			})
			.catch(response => {
				logger.info('delete venue error response', response.response);
				this.props.notify(response.response.data, {level: 'error'});
			});
	}

	handleDropZone(accepted, rejected) {
		logger.info('Handel Dropzone', accepted, rejected);

		this.setState({uploadMessage: ''});
		if (rejected.length > 0) {
			let file = rejected[0];
			let uploadMessage = 'Error:';
			if (file.size > this.logoMaxSize) uploadMessage += ' File is too large';
			if (this.logoAllowedTypes.indexOf(file.type) < 0) uploadMessage += ' File type is incorrect';
			this.setState({uploadMessage: uploadMessage});
			return;
		}

		if (accepted.length > 0) {
			let file = accepted[0];
			let fr = new FileReader();
			fr.onload = (event) => {

				this.setState({uploadMessage: ''});
			
				// Create fake event object and call the usual handler
				let base64encodedLogo = event.target.result;
				let fakeEvent = {};
				fakeEvent.target = {};
				fakeEvent.target.id = 'logo';
				fakeEvent.target.value = base64encodedLogo;
			
				this.handleVenueChange(fakeEvent);

				this.props.notify('Please log out and back in for the logo to be visible in the header');

			}
			fr.readAsDataURL(file);
		}
	}


	//
	// Term functions
	//
	displayTerm (termId, event) {
		logger.info('TERM SELECTED', termId, this.state);

		let selectedTerm;

		if (termId === 'none') {
			selectedTerm = {};
			selectedTerm.singleRateFormatted = '';
			selectedTerm.termRateFormatted = '';
			selectedTerm.isActive = false;

		} else {
			selectedTerm = this.state.terms.filter(term => term.id === termId)[0];
			selectedTerm.singleRateFormatted = (selectedTerm.singleRate / 100).toFixed(2) || null;
			selectedTerm.termRateFormatted = (selectedTerm.termRate / 100).toFixed(2) || null;
		}

		this.setState({
			selectedTerm: selectedTerm
		});
	}

	handleTermChange(event, whichDate) {

		let prevTerm = this.state.selectedTerm;
		let newValue = {};
		let debounceTimeout = 1000;

		if (!event) return;

		if (moment.isMoment(event)) {
			// A moment object is passed by the datepicker
			newValue[whichDate] = event;
		} else {

			newValue = {
				[event.target.id]: event.target.type === 'checkbox' ? event.target.checked : event.target.value
			};

			switch (event.target.id) {
				case 'singleRate':
				case 'termRate':
					newValue[event.target.id] = isNaN(event.target.value) ? 0 : newValue[event.target.id] * 100;
					newValue[event.target.id + 'Formatted'] = event.target.value;
					debounceTimeout = 2500;
					break;
			}
		}

logger.debug('PREVTERM', prevTerm, event.target, moment.isMoment(event));

		this.setState({selectedTerm: Object.assign({}, prevTerm, newValue)}, () => {

			// Validation
			logger.debug('VALIDATING+UPDATING', this.state.selectedTerm);

			debounceAxios(
				this.axiosInstance,
				'put',
				'/setting/update',
				{
					term: this.state.selectedTerm,
				},
				// Success callback
				response => {
					logger.debug('submit term success response', response);

					// Add a formatted price and update state
					if (response.data.term) {
						response.data.term.singleRateFormatted = (response.data.term.singleRate / 100).toFixed(2)
						response.data.term.termRateFormatted = (response.data.term.termRate / 100).toFixed(2)

						this.setState({selectedTerm: response.data.term});
					}

					// Update the dropdown
					this.getSettings();

					this.props.notify(
						'Term saved. There are ' + response.data.countInTerm + ' classes in this term');

					if (response.data.countNotInTerm) {
						this.props.notify(
							'Note there are ' + response.data.countNotInTerm + ' classes not in any term. ' +
							'Those classes don\'t have a rate and can\'t be booked', {level: 'info'});
					}

					// Find if terms are overlapping
					let previousTerm = {};
					this.state.terms.forEach(term => {
						if (term.endDate >= previousTerm.startDate) {
							this.props.notify('Term ' + term.name + ' overlaps with ' + previousTerm.name, {level: 'warning'});
						}
						previousTerm = term;
					});
				},
				// Error callback
				response => {
					logger.debug('submit term error response', response.response);
					this.props.notify(response, {level: 'error'});
				},
				// Timeout for debounce
				debounceTimeout,
			);
		});
	}

	handleTermRemove() {
		logger.info('Term remove', this.state.selectedTerm);

		let term = this.state.selectedTerm;
		if (!term) return;

		// Remove the term from the db
		this.axiosInstance.put('/setting/delete', {
			term: term,
		})
			.then(response => {
				logger.info('delete term success response', response);

				// Remove the term from state
				let newTerms = this.state.terms.filter(t => {
					return t.id !== term.id;
				});
				this.setState({
					selectedTerm: {},
					terms: newTerms,
				});

				let suffix = response.data.countInTerm ? '. There were ' + response.data.countInTerm + ' classes in that term' : '';
				this.props.notify('Term removed' + suffix);
			})
			.catch(response => {
				logger.info('delete term error response', response.response);
				this.props.notify('Removing term failed: ' + response.response.data, {level: 'error'});
			});
	}

	//
	// General functions
	//
	getSettings(props) {
		logger.info('SETTINGSGETTINGSETTINGS', props);

		let token = props && props.token;
		if (!token) return;

		let environment = props && props.environment;
		if (_.isEmpty(environment)) return;

		// Get my the school settings and list of locations
		this.axiosInstance = axios.create({
			baseURL: environment.baseUri,
			headers: {token: token}
		});
		this.axiosInstance.post('/settings')
			.then(response => {

				let newTerms = response.data.terms;
				newTerms.sort((a,b) => {                     // sort the terms on start date
					return a.startDate > b.startDate ? -1 : 1;
				});
				logger.info('settings success response', response, newTerms);

				this.setState({
					school: response.data.school,
					venues: response.data.venues,
					terms: newTerms,
				});

			})
			.catch((response) => {
				logger.info('settings error response', response.response, response);
				this.props.notify(response.response.data, {level: 'error'});
			});
	}

	whichSettings(props) {

		logger.info('WHICH SETTINGS', props);

		if (!props) return;

		let settingsWhat = {
			settingsSchool: false,
			settingsVenues: false,
			settingsTerms: false,
			settingsBasins: false,
		};
		switch (props.route.detail) {
			case 'school':
				settingsWhat.settingsSchool = true;
				break;
			case 'venues':
				settingsWhat.settingsVenues = true;
				break;
			case 'terms':
				settingsWhat.settingsTerms = true;
				break;
			case 'basins':
				settingsWhat.settingsBasins = true;
				break;
		}
		this.setState(settingsWhat);

		// this.setState((prev, props) => {
		// 	let state = {
		// 		person: props.person,
		// 		token: props.token,
		// 		venue: props.venue,
		// 	};
		// 	return state;
		// });
	}

	//
	// Renderers
	//
	render() {

		logger.info('RENDERING SETTINGS', this.state);

		const venuesDropdown = () => {

			let title = 'Choose Location...';
			let key='key';

			const allVenues = this.state.venues.map(venue => {
				return (
					<MenuItem
						key={ venue.id }
						eventKey={ venue.id }
						onSelect={ this.displayVenue }>
						{ venue.name }
					</MenuItem>
				);
			});

			return (
				<DropdownHoverButton bsStyle="slbprimary" title={title} key={key} id={'dropdown-basic'}>
					<MenuItem
						key='none'
						eventKey='none'
						onSelect={ this.displayVenue }>
						(None)
					</MenuItem>
					<MenuItem divider />
					{ allVenues }
				</DropdownHoverButton>
			)
		}

		const schoolForm = (
			<Form horizontal onChange={this.handleSchoolChange}>
				<FormGroup controlId="name">
					<Col componentClass={ControlLabel} sm={3}>Name</Col>
					<Col sm={9}><FormControl type="text" placeholder="School Name" value={this.state.school.name || ''} /></Col>
				</FormGroup>

				<FormGroup controlId="phoneNumber">
					<Col componentClass={ControlLabel} sm={3}>Phone</Col>
					<Col sm={9}><FormControl type="text" placeholder="Phone number" value={this.state.school.phoneNumber || ''}/></Col>
				</FormGroup>

				<FormGroup controlId="website">
					<Col componentClass={ControlLabel} sm={3}>Website</Col>
					<Col sm={9}><FormControl type="url" placeholder="http://www..." value={this.state.school.website || ''}/></Col>
				</FormGroup>

				<FormGroup controlId="abn">
					<Col componentClass={ControlLabel} sm={3}>ABN</Col>
					<Col sm={9}><FormControl type="text" placeholder="Your ABN is 11 numbers" value={this.state.school.abn || ''}/></Col>
				</FormGroup>

			</Form>
		);

		const venuesForm = (
			<Form horizontal onChange={this.handleVenueChange}>
				<FormGroup controlId="name">
					<Col componentClass={ControlLabel} sm={3}>Name</Col>
					<Col sm={9}><FormControl type="text" placeholder="Location Name" value={this.state.selectedVenue.name || ''} /></Col>
				</FormGroup>

				<FormGroup controlId="address1">
					<Col componentClass={ControlLabel} sm={3}>Address line 1</Col>
					<Col sm={9}><FormControl type="text" placeholder="E.g. street" value={this.state.selectedVenue.address1 || ''}/></Col>
				</FormGroup>

				<FormGroup controlId="address2">
					<Col componentClass={ControlLabel} sm={3}>Address line 2</Col>
					<Col sm={9}><FormControl type="text" placeholder="E.g. suburb or town" value={this.state.selectedVenue.address2 || ''}/></Col>
				</FormGroup>

				<FormGroup controlId="address3">
					<Col componentClass={ControlLabel} sm={3}>Address line 3</Col>
					<Col sm={9}><FormControl type="text" placeholder="E.g. state / postcode" value={this.state.selectedVenue.address3 || ''}/></Col>
				</FormGroup>

				<FormGroup controlId="phoneNumber">
					<Col componentClass={ControlLabel} sm={3}>Phone</Col>
					<Col sm={9}><FormControl type="text" placeholder="Phone Number" value={this.state.selectedVenue.phoneNumber || ''}/></Col>
				</FormGroup>

				<FormGroup controlId="email">
					<Col componentClass={ControlLabel} sm={3}>Email</Col>
					<Col sm={9}><FormControl type="email" placeholder="Email" value={this.state.selectedVenue.email || ''}/></Col>
				</FormGroup>

				<FormGroup controlId="website">
					<Col componentClass={ControlLabel} sm={3}>Website</Col>
					<Col sm={9}><FormControl type="url" placeholder="http://www..." value={this.state.selectedVenue.website || ''}/></Col>
				</FormGroup>

				<FormGroup controlId="abn">
					<Col componentClass={ControlLabel} sm={3}>ABN</Col>
					<Col sm={9}><FormControl type="text" placeholder="Your ABN is 11 numbers" value={this.state.selectedVenue.abn || ''}/></Col>
				</FormGroup>

				<FormGroup controlId="bsb">
					<Col componentClass={ControlLabel} sm={3}>Bank BSB</Col>
					<Col sm={9}><FormControl type="text" placeholder="BSB is 6 numbers 000-000" value={this.state.selectedVenue.bsb || ''}/></Col>
				</FormGroup>

				<FormGroup controlId="accountNumber">
					<Col componentClass={ControlLabel} sm={3}>Account Nr</Col>
					<Col sm={9}><FormControl type="text" placeholder="Account Number" value={this.state.selectedVenue.accountNumber || ''}/></Col>
				</FormGroup>

				<FormGroup controlId="accountName">
					<Col componentClass={ControlLabel} sm={3}>Account Name</Col>
					<Col sm={9}><FormControl type="text" placeholder="Account Name" value={this.state.selectedVenue.accountName || ''}/></Col>
				</FormGroup>

				<FormGroup controlId="logo">
					<Col componentClass={ControlLabel} sm={3}>Logo image</Col>
					<Col sm={5}>
						<Dropzone name="logo"
						          accept="image/jpeg, image/png"
						          multiple={ false }
						          maxSize={ 1024 * 1024 }
						          style={{ width: '100%' }}
								  onDrop={ this.handleDropZone }>
							{({ isDragActive, isDragReject }) => {
								return "Drop a logo HERE. Must be of type .jpeg, .jpg or .png. Maximum size is 1 Mb, preferably smaller";
							}}
						</Dropzone>
					</Col>
					<Col sm={4}>
						{ this.state.uploadMessage }
						{ this.props.environment && this.props.environment.imagesRootUrl && this.state.selectedVenue.logo ?
							<Image src={ this.props.environment.imagesRootUrl + this.state.selectedVenue.logo } responsive thumbnail/>
						: null }
					</Col>
				</FormGroup>

				<Col smOffset={3}>
					<ButtonGroup bsSize="sm">
						<Button bsStyle="slbneutral"  type="button" onClick={this.handleVenueRemove}><Icon glyph='icon-fontello-trash-1' /> Remove Location</Button>
					</ButtonGroup>
				</Col>

			</Form>
		);

		const termsDropdown = () => {

			let title = 'Choose Term...';
			let key='key';

			const allTerms = this.state.terms.map(term => {
				return (
					<MenuItem
						key={ term.id }
						eventKey={ term.id }
						onSelect={ this.displayTerm }>
						{ term.name }
					</MenuItem>
				);
			});

			return (
				<DropdownHoverButton bsStyle="slbprimary" title={title} key={key} id={'dropdown-basic'}>
					<MenuItem
						key='none'
						eventKey='none'
						onSelect={ this.displayTerm }>
						(New)
					</MenuItem>
					<MenuItem divider />
					{ allTerms }
				</DropdownHoverButton>
			)
		}

		const termsForm = (
			<Form horizontal onChange={this.handleTermChange}>
				<FormGroup controlId="name">
					<Col componentClass={ControlLabel} sm={3}>Term Name</Col>
					<Col sm={9}><FormControl type="text" placeholder="Term Name" value={this.state.selectedTerm.name || ''} /></Col>
				</FormGroup>

				<FormGroup controlId="phoneNumber">
					<Col componentClass={ControlLabel} sm={3}>Start Date</Col>
					<Col sm={9}>
	 					<DatePicker id="startDate"
	 					            onChange={ date => this.handleTermChange(date, 'startDate') }
	 					            selected={ this.state.selectedTerm.startDate ? moment(this.state.selectedTerm.startDate) : null }
	 					            dateFormat="DD-MM-YYYY"
	 					            placeholderText="Start date" />
					</Col>
				</FormGroup>

				<FormGroup controlId="website">
					<Col componentClass={ControlLabel} sm={3}>End Date</Col>
					<Col sm={9}>
						<DatePicker id="endDate"
						            onChange={ date => this.handleTermChange(date, 'endDate') }
						            selected={ this.state.selectedTerm.endDate ? moment(this.state.selectedTerm.endDate) : null }
						            dateFormat="DD-MM-YYYY"
						            placeholderText="End date" />
					</Col>
				</FormGroup>

				<FormGroup controlId="singleRate">
					<Col componentClass={ControlLabel} sm={3}>Single Price</Col>
					<Col sm={9}>
						<InputGroup>
							<InputGroup.Addon>$</InputGroup.Addon>
							<FormControl disabled={ this.state.selectedTerm.isActive }
							             type='text'
							             placeholder='00.00'
							             onChange={ this.handleTermChange }
							             value={ this.state.selectedTerm.singleRateFormatted }
							/>
						</InputGroup>
						{ this.state.selectedTerm.isActive
							? <span style={{fontSize: 'smallest'}}><i>Prices are fixed during the term or if swimmers are enrolled</i></span>
							: '' }
					</Col>
				</FormGroup>

				<FormGroup controlId="termRate">
					<Col componentClass={ControlLabel} sm={3}>Term Price</Col>
					<Col sm={9}>
						<InputGroup>
							<InputGroup.Addon>$</InputGroup.Addon>
							<FormControl disabled={ this.state.selectedTerm.isActive }
							             type='text'
							             placeholder='00.00'
							             onChange={ this.handleTermChange }
							             value={ this.state.selectedTerm.termRateFormatted }
							/>
						</InputGroup>
					</Col>
				</FormGroup>

				<Col smOffset={3}>
					<ButtonGroup bsSize="sm">
						<Button bsStyle="slbneutral"  type="button" onClick={this.handleTermRemove}><Icon glyph='icon-fontello-trash-1' /> Remove Term</Button>
					</ButtonGroup>
				</Col>

			</Form>
		);

		const termsList = () => {

			if (!Array.isArray(this.state.terms) || this.state.terms.length == 0 || !this.state.terms[0] ) return (<div/>);

			// let previousTerm = {};
			let theList = this.state.terms.map(term => {

				if (term) {

					let currentTerm = term.isCurrent ? 'NOW' : '';
					return (
							<Row className='term' id={term.id} key={term.id}>
								<Col sm={3} collapseLeft collapseRight>{ term.name + ' (' + moment(term.endDate).diff(term.startDate, 'weeks') + 'w)' } <span>&nbsp;</span><Label>{currentTerm}</Label></Col>
								<Col sm={2} collapseLeft collapseRight>{ moment(term.startDate).format('DD-MM-YYYY') }</Col>
								<Col sm={2} collapseLeft collapseRight>{ moment(term.endDate).format('DD-MM-YYYY') }</Col>
								<Col sm={2} collapseLeft collapseRight>{ term.singleRate ? '$' + (term.singleRate/ 100).toFixed(2) : '' }</Col>
								<Col sm={2} collapseLeft collapseRight>{ term.termRate ? '$' + (term.termRate/ 100).toFixed(2) : '' }</Col>
							</Row>
					)
				}
			});
			return theList;
		}

		return (
			<div>
				{/* Panel for School settings */}
				<PanelContainer className={this.state.settingsSchool ? '' : 'hide'}>
					<Panel>
						<PanelBody style={{padding: 0}}>
							<Grid>
								<Row>
									<Col xs={18} sm={12} md={6}>
										<Col xs={18} sm={12} md={12}>
											{ schoolForm }
											<p></p>
										</Col>
									</Col>
								</Row>
							</Grid>
						</PanelBody>
					</Panel>
				</PanelContainer>

				{/* Panel for Venues settings */}
				<PanelContainer className={this.state.settingsVenues ? '' : 'hide'}>
					<Panel>
						<PanelBody style={{padding: 0}}>
							<Grid>
								<Row>
									<Col xs={18} sm={12} md={6}>
										<Col xs={18} sm={12} smOffset={3} md={12} mdOffset={2}>
											{ venuesDropdown() }
											<p></p>
										</Col>
										<Col xs={18} sm={12} md={12}>
											{ venuesForm }
										</Col>
									</Col>
								</Row>
							</Grid>
						</PanelBody>
					</Panel>
				</PanelContainer>

				{/* Panel for Terms settings */}
				<PanelContainer className={this.state.settingsTerms ? '' : 'hide'}>
					<Panel>
						<PanelBody style={{padding: 0}}>
							<Grid>
								<Row>
									<Col xs={18} sm={12} md={5}>
										<Col xs={18} sm={12} smOffset={3} md={12} mdOffset={2}>
											{ termsDropdown() }
											<p></p>
										</Col>
										<Col xs={18} sm={12} md={12}>
											{ termsForm }
										</Col>
									</Col>

									<Col xs={18} sm={12} md={7}>
										<h4>Terms</h4>
										{ termsList() }
									</Col>

								</Row>
							</Grid>
						</PanelBody>
					</Panel>
				</PanelContainer>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return state;
}
const mapDispatchToProps = dispatch => {
	return {
		notify: (message, options) => dispatch(Action.notify(message, options)),
	}
}

Settings = connect (
	mapStateToProps,
	mapDispatchToProps,
)(Settings)

export default Settings


