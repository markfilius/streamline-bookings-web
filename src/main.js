import React from 'react';
import routes, { App } from './routes';
import render from '@sketchpixy/rubix/lib/node/router';

// import * as logger from './common/logger';

render(routes, () => {
	// logger.info('Completed rendering');
});

if (module.hot) {
	module.hot.accept('./routes', () => {

		// reload routes again
		require('./routes').default;

		render(routes, () => {
			// logger.info('Completed hot rendering');
		});
	});
}
